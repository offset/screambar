Short:        Screenbar plug-in for AmiNetRadio
Author:       offset@cpcscene.net (Philippe Rimauro)
Uploader:     offset@cpcscene.net (Philippe Rimauro)
Type:         util/wb
Version:      1.3
Requires:     AmiNetRadio.
Architecture: ppc-morphos >= 3.16



A useful screenbar module plug-in (and more!) for AmiNetRadio
by Philippe 'OffseT' Rimauro



Introduction
------------

AmiNetRadio is a damn good player with a lot of marvellous options and supported
formats... but it is quite old and does not support the latest MorphOS features,
such as the screenbar modules.

Here comes the first scream! With ScreamBar, AmiNetRadio will now have its own
screenbar module, displaying a small scope and all the desired control buttons!

But now you can scream again, because ScreamBar also adds support for
MagicBeacon! Any new playing track coming from AmiNetRadio will be automagically
notified to MagicBeacon to display some nice bubble or whatever you want!

And the scream is so long that ScreamBar was now turned into a nice launcher so
that AmiNetRadio will no longer complain about being run twice and will be
automatically brought to front at startup when needed.



History
-------

Version 1.3 (xx.xx.202x)
  - Added german translation (thanks to Stefan 'polluks' Haubenthal).

Version 1.2 (26.05.2022)
  - Fixed a possible buffer overflow during tooltypes management.
  - Updated to use the new magicbeacon.library from MorphOS 3.16 instead of the
    lowlevel message port.
  - Minors visual fixes in screenbar preferences.

Version 1.1 (11.04.2018)
  - Specify the list view & mode for hyperlinks related to files.
  - ScreamBar can now be used as a (smart) launcher for AmiNetRadio (eeek!).
  - Added middle mouse button on equalizer to bring AmiNetRadio to front.
  - Now HIDE then SHOW AmiNetRadio to bring it to front anycase.
  - All AREXX commands are now sent to AmiNetRadio from the main task rather
    than the screen bar itself.
  - If launched without parameters and if AmiNetRadio is already running,
    ScreamBar will bring it to front.
  - Fixed AppIcon to use ScreamBar executable icon.

Version 1.0 (23.12.2014)
  - First release.
 


Requirements:
-------------

MorphOS 3.16 or better.
A working AmiNetRadio installation.



Installation:
-------------

1. Copy the ScreamBar executable into the "plugins" drawer of AmiNetRadio.

2. Optionally, also copy the "Catalogs" drawer to the "plugins" drawer of
   AmiNetRadio for French or Dutch translations (cd file is available in the
   drawer if you want to create additional ones).

3. Optionally, in MagicBeacon, configure a notification type "TrackNext"
   (registered under the application name "ScreamBar") to decide what to do
   when a new track is being played in AmiNetRadio (display a bubble with the
   message "Now playing: %m" could be a good idea).

4. Optionally, configure ScreamBar as your favorite player in Ambient MIME and
   it will automagically start AmiNetRadio with the provided media or make it
   switch to this media if it is already running (works also from CLI and from
   project icons).

Scream! That's it!



Usage:
------

Launch directly ScreamBar (from the "plugins" drawer) and it will also launch
AmiNetRadio for you.

or

Launch AmiNetRadio and start the ScreamBar plug-in from the "Visual Plugins"
menu.



Tips:
-----

1. Leave the mouse on the scope to display a bubble with detailed information
   about the currently playing media.

2. Double click on the scope from the screenbar to open a small window about
   the currently playing media, and optionally quit ScreamBar plug-in.

3. On the scope, hold left mouse button pressed, then press right mouse button
   to bring AmiNetRadio o front (also now works with a single press on middle
   mouse button).

4. Use the screenbar preferences to configure the look and feel you prefer for
   ScreamBar.



Bugs:
-----

AmiNetRadio behaves quite weird with some exotic players; notifications are not
the expected ones and even worse, information coming from its AREXX port might
be wrong at some point.

I did my best to put in place workarounds in ScreamBar's notification handling
to avoid as much as possible unexpected/missing messages. But in some cases,
it will not work properly and there is nothing I can do about it (the players
and/or AmiNetRadio should be fixed).

Anyway, in most cases, ScreamBar will do a kind of magic and everything should
work nicely.



Credits
-------

Code        : Philippe 'OffseT' Rimauro
Icons       : David 'davebraco' Braconnier
Translations: Philippe 'OffseT' Rimauro
              Xavier 'Alta�r' Lerot
              Stefan 'polluks' Haubenthal

http://ace.cpcscene.net
http://quasar.cpcscene.net
http://futurs.cpcscene.net
