/********************************************************************
 *                                                                  *
 * ScreamBar - Screenbar for AmiNetRadio                            *
 * by OffseT of Futurs' using SimpleCat (c) 2006 by Guido Mersmann  *
 *                                                                  *
 ** SimpleLocale.c ********************** Simple Locale Management **/

/* /// "Includes" */

#include <proto/locale.h>

#include "SimpleLocale.h" /* prototypes and catcomp block */

/* /// */

/* /// "Local variable" */

struct Locale  *locale_locale  = NULL;
struct Catalog *locale_catalog = NULL;

/* /// */

/* /// "Open & close functions" */

BOOL LocaleOpen(STRPTR catname, ULONG version, ULONG revision)
{
	if((locale_locale = OpenLocale(NULL)))
    {
		if((locale_catalog = OpenCatalogA(locale_locale, catname, TAG_DONE)))
        {
			if(locale_catalog->cat_Version >= version
            || (locale_catalog->cat_Version == version && locale_catalog->cat_Revision >= revision))
            {
                return TRUE;
            }
			CloseCatalog(locale_catalog);
			locale_catalog = NULL;
        }
        return TRUE;
	}
	return FALSE;
}

VOID LocaleClose()
{
    if(locale_catalog)
    {
        CloseCatalog(locale_catalog);
        locale_catalog = NULL;
    }
    if(locale_locale)
    {
        CloseLocale(locale_locale);
        locale_locale = NULL;
    }
}

/* /// */

/* /// "Translation functions" */

STRPTR GetString(LONG id)
{
    LONG   *l;
    UWORD  *w;
    STRPTR  builtin;

	l = (LONG *)CatCompBlock;

    while(*l != id )
    {
        w = (UWORD *)((ULONG)l + 4);
        l = (LONG *)((ULONG)l + (ULONG)*w + 6);
    }
    builtin = (STRPTR)((ULONG)l + 6);

    if(locale_catalog)
    {
        return (APTR)GetCatalogStr(locale_catalog, id, builtin);
    }
    return builtin;
}

VOID LocalizeMenu(struct NewMenu menu[])
{
    LONG i = 0;

    while(menu[i].nm_Type != NM_END)
    {
        if((LONG)menu[i].nm_Label >= 0)
            menu[i].nm_Label = GetString((LONG)menu[i].nm_Label);

        i++;
    }
}

VOID LocalizeStringArray(STRPTR array[])
{
    LONG i = 0;

    while(array[i])
    {
        array[i] = GetString((LONG)array[i]);
        i++;
    }
}
/* /// */

