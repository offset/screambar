/*
** ScreamBar visual plug-in for AmiNetRadio
** (c) 2014-2022 by Philippe 'OffseT' Rimauro
*/

/* /// "Includes" */

//#define INTUI_V50_NOOBSOLETE

#include <proto/exec.h>
#include <proto/dos.h>
#include <proto/utility.h>
#include <proto/graphics.h>
#include <proto/intuition.h>
#include <proto/icon.h> // Do not load with INTUI_V50_NOOBSOLETE!
#include <proto/rexxsyslib.h>
#include <proto/muimaster.h>
#include <proto/charsets.h>

#include <clib/alib_protos.h>
#include <clib/debug_protos.h>

#include <exec/types.h>
#include <exec/memory.h>
#include <exec/rawfmt.h>
#include <dos/dos.h>
#include <dos/dostags.h>
#include <graphics/gfxbase.h>
#include <graphics/rpattr.h>
#include <intuition/screenbar.h>
#include <intuition/extensions.h>
#include <rexx/errors.h>
#include <libraries/charsets.h>
#include <libraries/mui.h>

#include <mui/Rawimage_mcc.h>
#include <mui/Crawling_mcc.h>
#include <mui/Hyperlink_mcc.h>

#include <string.h>

#include "SmartReadArgs.h"
#include "SimpleLocale.h"
#include "TrackInfo.h"

#define USE_INLINE_STDARG
#include <proto/magicbeacon.h>
#undef USE_INLINE_STDARG
#include <libraries/magicbeacon.h>

struct Library *MagicBeaconBase = NULL;

/* /// */

/* /// "Externs" */

extern const unsigned char RawImg_Logo[];
extern const unsigned char RawImg_Prefs[];

extern const unsigned char RawImg_Play[];
extern const unsigned char RawImg_Pause[];
extern const unsigned char RawImg_Stop[];
extern const unsigned char RawImg_Next[];
extern const unsigned char RawImg_Previous[];

/* /// */

/* /// "Application defines" */

// Application identifiers
#define PROGRAM_VER 1
#define PROGRAM_REV 1

#define PROGRAM_NAME      "ScreamBar"
#define VERSION_STRING    "1.3"
#define COMPILATION_DATE  "xx.xx.202x"

#define PROGRAM_AUTHOR    "Philippe Rimauro"
#define PROGRAM_PORT      "ScreamBar"
#define PROGRAM_EXE       "ScreamBar"
#define PROGRAM_CATALOG   "ScreamBar.catalog"

#define PROGRAM_COPYRIGHT     "� 2014-202x " PROGRAM_AUTHOR
#define PROGRAM_VERSION_SHORT PROGRAM_NAME " " VERSION_STRING " (" COMPILATION_DATE ") "
#define PROGRAM_VERSION       "$VER: " PROGRAM_VERSION_SHORT "" PROGRAM_COPYRIGHT

/* /// */

/* /// "Convinience macros" */

#define DISPATCHERNAME_BEGIN(Name)          \
ULONG Name(void) { Class *cl=(Class*) REG_A0; Msg msg=(Msg) REG_A1; Object *obj=(Object*) REG_A2; switch (msg->MethodID) {

#define DISPATCHER_END } return DoSuperMethodA(cl,obj,msg);}

#define DISPATCHER_GATE(Name) struct EmulLibEntry _Dispatcher_##Name = { TRAP_LIB, 0, (void (*)(void)) Name };
#define DISPATCHER_REF(Name) &_Dispatcher_##Name

// IO macros
#define IO_SIGBIT(req)	   ((LONG)(((struct IORequest *)req)->io_Message.mn_ReplyPort->mp_SigBit))
#define IO_SIGMASK(req)    ((LONG)(1L<<IO_SIGBIT(req)))
#define PORT_SIGMASK(port) ((LONG)(1L<<port->mp_SigBit))

#define UNUSED __attribute__ ((__unused__))

#define RESET_STRING(string, reset) CopyMem(reset, string, strlen(reset)+1)

/* /// */

/* /// "Plug-in interface structure" */

struct PluginMessage
{
    struct Message     msg;
    ULONG              PluginMask;
    struct Process    *PluginTask;
    UWORD            **SpecRawL;
    UWORD            **SpecRawR;
    WORD               Accepted;  /* v1.3: Don't use BOOL any more because */
    WORD               reserved0; /* it causes alignment problems!         */

    /* All data beyond this point is new for v1.2.                         */
    /* AmigaAMP v2.5 and up will detect this new data and act accordingly. */
    /* Older versions of AmigaAMP will simply ignore it.                   */

    ULONG              InfoMask;
    struct TrackInfo **tinfo;
    struct MsgPort    *PluginWP;  /* v1.4 (used to be reserved)            */
                                  /* If not used set to NULL to avoid      */
                                  /* unnessecary overhead!                 */

    /* All data beyond this point is new for v1.3.                         */
    /* AmigaAMP v2.7 and up will detect this new data and act accordingly. */
    /* Older versions of AmigaAMP will simply ignore it.                   */

    WORD             **SampleRaw;
};

/* /// */

/* /// "Main data structure" */

#define MAX_FIELD_SIZE 256
#define MAX_MESSAGE_SIZE (MAX_FIELD_SIZE*6*2)
#define AUDIO_DATA_SIZE 512

struct ApplicationData
{
    // Cross task
    Object *MyApp;
    ULONG MySignal;
    WORD AudioData[AUDIO_DATA_SIZE];
    UBYTE DisplayMessage[MAX_MESSAGE_SIZE];
    BOOL IsDisplayed;
    // Main task only
    Object *MyWin;
    Object *Stream;
    Object *Title;
    Object *Artist;
    Object *Album;
    Object *Year;
    Object *Genre;
    Object *Equalizer;
    ULONG PSMID;
    APTR BeaconHandle;
    struct MsgPort *ANR_ReplyPort;
    struct RexxMsg *ANR_RxMsg;
    UWORD LastOperationMode;
    UBYTE LastTrackStream[MAX_FIELD_SIZE];
    UBYTE LastTrackTitle[MAX_FIELD_SIZE];
    UBYTE LastTrackArtist[MAX_FIELD_SIZE];
    UBYTE LastTrackAlbum[MAX_FIELD_SIZE];
    UBYTE LastTrackYear[MAX_FIELD_SIZE];
    UBYTE LastTrackGenre[MAX_FIELD_SIZE];
};

/* /// */

/* /// "MUI custom classes structures" */

struct ApplicationClassData
{
    ULONG dummy;
};

struct ScreamBarClassData
{
    struct ApplicationData *AppData;
    BOOL PlayDisplayed;
    BOOL SlowMode;

    Object *PlayPause;
    Object *Equalizer;
    Object *EquGroup;
    Object *BtnGroup;
    Object *TopMargin;
    Object *BottomMargin;
};

struct EqualizerClassData
{
    // Application
    Object *MasterApp;
    // Audio data source
    BYTE  *AudioData;
    CONST_STRPTR DisplayMessage;
    // Configurable stuff
    ULONG Pen;
    ULONG Paper;
    BOOL  IsTransparent;
    ULONG Width;
    // Inner event stuff
    struct MUI_EventHandlerNode eh;
    ULONG Seconds;
    ULONG Micros;
    BOOL LMBPressed;
};

struct MUIP_Application_SendRxMsg
{
    ULONG id;
    STRPTR command;
};

/* /// */

/* /// "Prototypes" */

static WORD PluginInit(VOID);
static VOID PluginExit(VOID);
static VOID PluginLoop(VOID);
static VOID ShowRequester(char *Text, char *Button);
static STRPTR SendRxMsgToANR(STRPTR dataName, struct RexxMsg *rxMsg, struct MsgPort *replyPort);
static VOID SendRxMsg(CONST_STRPTR portName, struct RexxMsg *rxMsg, struct MsgPort *replyPort);
static Object * MakeButton(CONST_STRPTR label, CONST_STRPTR help);

/* /// */

/* /// "Visual plug-in global variables" */

static ULONG            InfoMask;
static ULONG            PluginMask;
static WORD             Accepted;
static struct Process   *PluginTask;
static struct MsgPort   *PluginMP;
static struct MsgPort   *PluginRP;
static struct TrackInfo *tinfo;

static UWORD *SpecRawL;
static UWORD *SpecRawR;
static WORD  *SampleRaw;

/* /// */

/* /// "Application global variables" */

static struct MUI_CustomClass *ApplicationClass = NULL;
static struct MUI_CustomClass *ScreenbarClass = NULL;
static struct MUI_CustomClass *EqualizerClass = NULL;

static struct ApplicationData *AppData;

/* /// */

/* /// "Classes defines" */

/*
** NOTE: do NOT use this serialno in your applications! ask for your own serial number!
*/
#define MUISERIALNO_OFFSET 0xfebb

#define RETURNID_OPENWINDOW 1

#define CFGID_PEN_COLOR      MUI_CFGID(MUISERIALNO_OFFSET,1 /* Textual */,1)
#define CFGID_PAPER_COLOR    MUI_CFGID(MUISERIALNO_OFFSET,1 /* Textual */,2)
#define CFGID_IS_TRANSPARENT MUI_CFGID(MUISERIALNO_OFFSET,0 /* Numeric */,3)
#define CFGID_TOP_MARGIN     MUI_CFGID(MUISERIALNO_OFFSET,0 /* Numeric */,4)
#define CFGID_BOTTOM_MARGIN  MUI_CFGID(MUISERIALNO_OFFSET,0 /* Numeric */,5)
#define CFGID_WIDTH          MUI_CFGID(MUISERIALNO_OFFSET,0 /* Numeric */,6)
#define CFGID_SLOW_MODE      MUI_CFGID(MUISERIALNO_OFFSET,0 /* Numeric */,7)
#define CFGID_BUTTONS_STATUS MUI_CFGID(MUISERIALNO_OFFSET,0 /* Numeric */,8)

#define DEFAULT_PEN_COLOR   "r001133"
#define DEFAULT_PAPER_COLOR "r116699"
#define DEFAULT_TOP_MARGIN     4
#define DEFAULT_BOTTOM_MARGIN  5
#define DEFAULT_WIDTH         50

#define MUIA_Equalizer_MasterApp      TAG_USER | 0x0001 // [I..] Object * Application to send notifications to
#define MUIA_Equalizer_AudioData      TAG_USER | 0x0002 // [I..] BYTE * pointing to the area with the data for the equalizer - MANDATORY! (must remain alway valid)
#define MUIA_Equalizer_DisplayMessage TAG_USER | 0x0003 // [I..] CONST_STRPTR pointing to the area with the string for the shorthelp (must remain alwy valid)
#define MUIA_Equalizer_Pen            TAG_USER | 0x0004 // [.SG]
#define MUIA_Equalizer_Paper          TAG_USER | 0x0005 // [.SG]
#define MUIA_Equalizer_IsTransparent  TAG_USER | 0x0007 // [.SG]
#define MUIA_Equalizer_Width          TAG_USER | 0x0008 // [.SG]

#define MUIM_Application_SendRxMsg TAG_USER | 0x0001
#define MUIM_Application_PlayPause TAG_USER | 0x0002

/* /// */

/* /// "MCC Application class" */

static ULONG Application_SendRxMsg(UNUSED struct IClass *cl, UNUSED Object *obj, struct MUIP_Application_SendRxMsg *msg)
{
    struct MsgPort *replyPort = NULL;
    struct RexxMsg *rxMsg = NULL;

    if((replyPort = CreateMsgPort()))
    {
		rxMsg = CreateRexxMsg(replyPort,NULL,NULL);

    	rxMsg->rm_Args[0] = msg->command;
        rxMsg->rm_Action = RXCOMM | RXFF_RESULT;

        SendRxMsg("AMINETRADIO.1", rxMsg, replyPort);
    }

    if(rxMsg) DeleteRexxMsg(rxMsg);
    if(replyPort) DeleteMsgPort(replyPort);

    return 0;
}

static ULONG Application_PlayPause(UNUSED struct IClass *cl, UNUSED Object *obj, UNUSED Msg msg)
{
    struct MsgPort *replyPort = NULL;
    struct RexxMsg *rxMsg = NULL;

    if((replyPort = CreateMsgPort()))
    {
		rxMsg = CreateRexxMsg(replyPort,NULL,NULL);

    	rxMsg->rm_Args[0] = "PLAY STATUS";
        rxMsg->rm_Action = RXCOMM | RXFF_RESULT;

        SendRxMsg("AMINETRADIO.1", rxMsg, replyPort);

        if(rxMsg->rm_Result1 == RC_OK         // Status was received
        && *(STRPTR)rxMsg->rm_Result2 == '0') // Status is stopped
        	rxMsg->rm_Args[0] = "PLAY";
        else
        	rxMsg->rm_Args[0] = "PAUSE";

        rxMsg->rm_Action = RXCOMM | RXFF_RESULT;
        SendRxMsg("AMINETRADIO.1", rxMsg, replyPort);
    }

    if(rxMsg) DeleteRexxMsg(rxMsg);
    if(replyPort) DeleteMsgPort(replyPort);

    return 0;
}

DISPATCHERNAME_BEGIN(MCC_Application)
    case MUIM_Application_SendRxMsg : return Application_SendRxMsg(cl,obj,(APTR)msg);
    case MUIM_Application_PlayPause : return Application_PlayPause(cl,obj,(APTR)msg);
DISPATCHER_END
DISPATCHER_GATE(MCC_Application)

/* /// */

/* /// "MCC Equalizer class" */

static ULONG Equalizer_New(struct IClass *cl,Object *obj,struct opSet *msg)
{
	struct TagItem *tags,*tag;
    Object *masterApp = NULL;
    BYTE *audioData = NULL;
    CONST_STRPTR displayMessage = NULL;

	// Parse initial taglist
	for(tags=((struct opSet *)msg)->ops_AttrList; (tag=NextTagItem(&tags)); )
	{
		switch(tag->ti_Tag)
		{
			case MUIA_Equalizer_MasterApp:
                masterApp = (Object *)tag->ti_Data;
    			break;
			case MUIA_Equalizer_AudioData:
                audioData = (BYTE *)tag->ti_Data;
    			break;
            case MUIA_Equalizer_DisplayMessage:
                displayMessage = (CONST_STRPTR)tag->ti_Data;
                break;
		}
    }

    obj = DoSuperNew(cl,obj,
        NoFrame,
        MUIA_FillArea,FALSE,
        MUIA_DoubleBuffer,TRUE,
        MUIA_ShortHelp, "dummy", // Required to enable custom shorthelp
        TAG_MORE,msg->ops_AttrList);

    if(audioData && obj)
    {
        struct EqualizerClassData *data = INST_DATA(cl,obj);

        data->MasterApp = masterApp;
        data->AudioData = audioData;
        data->DisplayMessage = displayMessage;
        data->Pen   = -1;
        data->Paper = -1;
        data->IsTransparent = TRUE;
        data->Width = DEFAULT_WIDTH;
        data->Seconds = 0;
        data->Micros  = 0;
        data->LMBPressed = FALSE;

        return (ULONG)obj;
    }

	return 0;
}

static ULONG Equalizer_Setup(struct IClass *cl,Object *obj,Msg msg)
{
	struct EqualizerClassData *data = INST_DATA(cl,obj);

	if(!(DoSuperMethodA(cl,obj,msg)))
		return FALSE;

    // Install event handler
    data->eh.ehn_Priority = 0;
    data->eh.ehn_Flags = 0;
    data->eh.ehn_Events = IDCMP_MOUSEBUTTONS/*|IDCMP_MOUSEMOVE*/;
    data->eh.ehn_Object = obj;
    data->eh.ehn_Class = NULL;
    DoMethod(_win(obj),MUIM_Window_AddEventHandler,(ULONG)&data->eh);

	return TRUE;
}

static ULONG Equalizer_Cleanup(struct IClass *cl,Object *obj,Msg msg)
{
	struct EqualizerClassData *data = INST_DATA(cl,obj);

    DoMethod(_win(obj),MUIM_Window_RemEventHandler,(ULONG)&data->eh);

	return DoSuperMethodA(cl,obj,msg);
}

static ULONG Equalizer_Set(struct IClass *cl, Object *obj, Msg msg)
{
	struct EqualizerClassData *data = INST_DATA(cl,obj);
	struct TagItem *tags,*tag;

	for(tags=((struct opSet *)msg)->ops_AttrList; (tag=NextTagItem(&tags)); )
	{
		switch(tag->ti_Tag)
		{
            case MUIA_Equalizer_Pen:
                data->Pen = tag->ti_Data;
                break;
            case MUIA_Equalizer_Paper:
                data->Paper = tag->ti_Data;
                break;
            case MUIA_Equalizer_IsTransparent:
                data->IsTransparent = (BOOL)tag->ti_Data;
                break;
            case MUIA_Equalizer_Width:
                data->Width = tag->ti_Data;
                break;
    	}
    }

	return(DoSuperMethodA(cl,obj,msg));
}

static ULONG Equalizer_Get(struct IClass *cl,Object *obj,Msg msg)
{
	struct EqualizerClassData *data = INST_DATA(cl,obj);
	ULONG *store = ((struct opGet *)msg)->opg_Storage;

	switch(((struct opGet *)msg)->opg_AttrID)
	{
        case MUIA_Equalizer_Pen:
            *store = data->Pen;
            return TRUE;
        case MUIA_Equalizer_Paper:
            *store = data->Paper;
            return TRUE;
        case MUIA_Equalizer_IsTransparent:
            *store = (ULONG)data->IsTransparent;
            return TRUE;
        case MUIA_Equalizer_Width:
            *store = data->Width;
            return TRUE;
    }

	return(DoSuperMethodA(cl,obj,msg));
}

static ULONG Equalizer_CreateShortHelp(struct IClass *cl, Object *obj, UNUSED struct MUIP_CreateShortHelp *msg)
{
	struct EqualizerClassData *data = INST_DATA(cl,obj);

    return (ULONG)data->DisplayMessage;
}

// some helper macros
#define _between(a,x,b) ((x)>=(a) && (x)<=(b))
#define _isinobject(x,y) (_between(_mleft(obj),(x),_mright (obj)) \
                           && _between(_mtop(obj) ,(y),_mbottom(obj)))

static ULONG Equalizer_HandleEvent(struct IClass *cl,Object *obj,struct MUIP_HandleEvent *msg)
{
    struct EqualizerClassData *data = INST_DATA(cl,obj);

    if (msg->imsg != NULL)
    {
        switch (msg->imsg->Class)
        {
            case IDCMP_MOUSEBUTTONS:
                if(_isinobject(msg->imsg->MouseX,msg->imsg->MouseY))
                {
                    if(msg->imsg->Code == SELECTDOWN)
                    {
                        data->LMBPressed = TRUE;

                        if(data->MasterApp)
                        {
                            if(DoubleClick(data->Seconds, data->Micros, msg->imsg->Seconds, msg->imsg->Micros))
                            {
                                data->Seconds = 0;
                                data->Micros  = 0;

                                // NOTE: you MUST NOT call any objects of your applications directly!
                                // they are running on a different process.
                                // for more sophisticated tasks consider using message ports and
                                // a custom main loop
                                DoMethod(data->MasterApp,MUIM_Application_PushMethod,data->MasterApp,2,
                                    MUIM_Application_ReturnID,RETURNID_OPENWINDOW);
                            }
                            else
                            {
                                data->Seconds = msg->imsg->Seconds;
                                data->Micros  = msg->imsg->Micros;
                            }
                        }

                        return MUI_EventHandlerRC_Eat;
                    }
                    else if(msg->imsg->Code == SELECTUP)
                    {
                        data->LMBPressed = FALSE;
                    }
                    else if(msg->imsg->Code == MIDDLEDOWN
                         || (msg->imsg->Code == MENUDOWN && data->LMBPressed))
                    {
                        DoMethod(data->MasterApp,MUIM_Application_PushMethod,data->MasterApp,2,
                                 MUIM_Application_SendRxMsg,"HIDE");
                        DoMethod(data->MasterApp,MUIM_Application_PushMethod,data->MasterApp,2,
                                 MUIM_Application_SendRxMsg,"SHOW");
                        return MUI_EventHandlerRC_Eat;
                    }
                }
                break;
        }
    }

    return 0;
}

static ULONG Equalizer_AskMinMax(struct IClass *cl,Object *obj,struct MUIP_AskMinMax *msg)
{
    ULONG rc = DoSuperMethodA(cl,obj,msg);
    struct MUI_MinMax *mm = msg->MinMaxInfo;
    struct EqualizerClassData *data = INST_DATA(cl,obj);

    if(data->MasterApp)
    {
        // Mind that you should generally stick to fixed widths,
        // the custom layouter isn't very smart :)
        mm->MinWidth = mm->DefWidth = mm->MaxWidth = data->Width;

        // If you provide MinHeight that's bigger than the actual titlebar size,
        // your object will not be displayed in the titlebar!
        mm->MinHeight = mm->DefHeight = 3;
        mm->MaxHeight = _screen(obj)->BarHeight + 1;
    }
    else
    {
    	msg->MinMaxInfo->MinWidth  += 50;
    	msg->MinMaxInfo->DefWidth  += 50;
    	msg->MinMaxInfo->MaxWidth  += MUI_MAXMAX;

    	msg->MinMaxInfo->MinHeight += 10;
    	msg->MinMaxInfo->DefHeight += 50;
    	msg->MinMaxInfo->MaxHeight += MUI_MAXMAX;
    }

    return rc;
}

// Some helper macros
#define _between(a,x,b) ((x)>=(a) && (x)<=(b))
#define _isinobject(x,y) (_between(_mleft(obj),(x),_mright (obj)) \
                       && _between(_mtop(obj) ,(y),_mbottom(obj)))

static ULONG Equalizer_Draw(struct IClass *cl,Object *obj,struct MUIP_Draw *msg)
{
    ULONG rc = DoSuperMethodA(cl,obj,msg);

    if(msg->flags & (MADF_DRAWOBJECT | MADF_DRAWUPDATE))
    {
        struct EqualizerClassData *data = INST_DATA(cl,obj);
        LONG x;
        LONG midY = _mbottom(obj)-_height(obj)/2;
        LONG left = _mleft(obj)+_addleft(obj);
        LONG right = _mright(obj)-_addleft(obj);
        LONG top = _top(obj)+_addtop(obj);
        LONG width = _width(obj)-2*_addleft(obj);
        LONG height = _height(obj) - 2*_addtop(obj);

        if(data->IsTransparent || data->Paper == -1UL)
        {
            Object *parent = NULL;

            get(obj,MUIA_Parent,&parent);

            DoMethod(parent,MUIM_DrawBackground,left,top,width,height,0,0,0);
        }
        else
        {
            SetAPen(_rp(obj),MUIPEN(data->Paper));
            RectFill(_rp(obj),_mleft(obj),_mtop(obj),_mright(obj),_mbottom(obj));
        }
   
        if(data->Pen != -1UL)
        {
            SetAPen(_rp(obj),MUIPEN(data->Pen));
        }

        for(x=left; x<=right; x++)
        {
            ULONG idx = ((x-left)*(AUDIO_DATA_SIZE/2-1))/(width-1);

            Move(_rp(obj), x, midY);
            Draw(_rp(obj), x, midY + (height-1)*(AppData->AudioData[idx*2] + AppData->AudioData[idx*2+1])/(4*32768));
        }

        /*{
            STRPTR text = "This is a text!";
            LONG len = strlen(text);
            STRPTR preparse = "";
            ULONG flags = 0;

        	DoMethod(obj,MUIM_Text,left,top,width,height,text,len,preparse,flags);
        }*/
    }

    return rc;
}

DISPATCHERNAME_BEGIN(MCC_Equalizer)
    case OM_NEW                    : return Equalizer_New            (cl,obj,(APTR)msg);
    case OM_SET                    : return Equalizer_Set            (cl,obj,(APTR)msg);
    case OM_GET                    : return Equalizer_Get            (cl,obj,(APTR)msg);
    case MUIM_Setup                : return Equalizer_Setup          (cl,obj,(APTR)msg);
    case MUIM_Cleanup              : return Equalizer_Cleanup        (cl,obj,(APTR)msg);
    case MUIM_AskMinMax            : return Equalizer_AskMinMax      (cl,obj,(APTR)msg);
    case MUIM_Draw                 : return Equalizer_Draw           (cl,obj,(APTR)msg);
    case MUIM_CreateShortHelp      : return Equalizer_CreateShortHelp(cl,obj,(APTR)msg);
    case MUIM_HandleEvent          : return Equalizer_HandleEvent    (cl,obj,(APTR)msg);
DISPATCHER_END
DISPATCHER_GATE(MCC_Equalizer)

/* /// */

/* /// "MCC ScreamBar sbar class" */

static ULONG ScreamBar_New(struct IClass *cl,Object *obj,struct opSet *msg)
{
    Object *Equalizer;
    Object *EquGroup, *BtnGroup, *PlayPause;
    Object *Play, *Pause, *Stop, *Next, *Previous;

    // This is basically the only way to actually pass some data
    // into our instances without using ugly global variables.
    // Do consider using this!
    struct ApplicationData *appData = (struct ApplicationData *)cl->cl_UserData;

    obj = DoSuperNew(cl,obj,
        MUIA_Group_Horiz, TRUE,

        Child, BtnGroup = VGroup,
            GroupSpacing(0),
            Child, HVSpace,
            Child, HGroup,
                GroupSpacing(0),
                Child, Previous = RawimageObject,
                    MUIA_Rawimage_Data, RawImg_Previous,
                	MUIA_InputMode, MUIV_InputMode_RelVerify,
                    End,
                Child, PlayPause = PageGroup,
                    Child, Play = RawimageObject,
                        MUIA_Rawimage_Data, RawImg_Play,
                    	MUIA_InputMode, MUIV_InputMode_RelVerify,
                        End,
                    Child, Pause = RawimageObject,
                        MUIA_Rawimage_Data, RawImg_Pause,
                    	MUIA_InputMode, MUIV_InputMode_RelVerify,
                        End,
                    End,
                Child, Stop = RawimageObject,
                    MUIA_Rawimage_Data, RawImg_Stop,
                	MUIA_InputMode, MUIV_InputMode_RelVerify,
                    End,
                Child, Next = RawimageObject,
                    MUIA_Rawimage_Data, RawImg_Next,
                	MUIA_InputMode, MUIV_InputMode_RelVerify,
                    End,
                End,
            Child, HVSpace,
            End,

        Child, EquGroup = VGroup,
            GroupSpacing(0),
            Child, Equalizer = NewObject(EqualizerClass->mcc_Class,NULL,
                MUIA_Equalizer_MasterApp, (ULONG)appData->MyApp,
                MUIA_Equalizer_AudioData, (ULONG)appData->AudioData,
                MUIA_Equalizer_DisplayMessage, (ULONG)appData->DisplayMessage,
                TAG_DONE),
            End,

        TAG_MORE,msg->ops_AttrList);

    if(obj)
    {
        struct ScreamBarClassData *data = INST_DATA(cl,obj);

        data->AppData = appData;
        data->PlayDisplayed = TRUE;
        data->PlayPause = PlayPause;
        data->Equalizer = Equalizer;
        data->EquGroup = EquGroup;
        data->BtnGroup = BtnGroup;
        data->TopMargin = NULL;
        data->BottomMargin = NULL;
        data->SlowMode = FALSE;

        // Buttons clicked actions
        DoMethod(Play,MUIM_Notify,MUIA_Pressed,FALSE,
                 data->AppData->MyApp,4,MUIM_Application_PushMethod,data->AppData->MyApp,1,MUIM_Application_PlayPause);
        DoMethod(Pause,MUIM_Notify,MUIA_Pressed,FALSE,
                 data->AppData->MyApp,4,MUIM_Application_PushMethod,data->AppData->MyApp,1,MUIM_Application_PlayPause);

        DoMethod(Stop,MUIM_Notify,MUIA_Pressed,FALSE,
                 data->AppData->MyApp,5,MUIM_Application_PushMethod,data->AppData->MyApp,2,MUIM_Application_SendRxMsg,"STOP");
        DoMethod(Previous,MUIM_Notify,MUIA_Pressed,FALSE,
                 data->AppData->MyApp,5,MUIM_Application_PushMethod,data->AppData->MyApp,2,MUIM_Application_SendRxMsg,"PLAY PREV");
        DoMethod(Next,MUIM_Notify,MUIA_Pressed,FALSE,
                 data->AppData->MyApp,5,MUIM_Application_PushMethod,data->AppData->MyApp,2,MUIM_Application_SendRxMsg,"PLAY NEXT");
    }

    return (ULONG)obj;
}

static ULONG ScreamBar_Dispose(struct IClass *cl, Object *obj, Msg msg)
{
	return DoSuperMethodA(cl,obj,msg);
}

static ULONG ScreamBar_Setup(struct IClass *cl,Object *obj,Msg msg)
{
    ULONG rc = DoSuperMethodA(cl,obj,msg);

    if(rc)
    {
        struct ScreamBarClassData *data = INST_DATA(cl,obj);
        char *newpencolor = DEFAULT_PEN_COLOR;
        char *newpapercolor = DEFAULT_PAPER_COLOR;
        ULONG newistransparent = TRUE;
        ULONG newtopmargin = DEFAULT_TOP_MARGIN;
        ULONG newbottommargin = DEFAULT_BOTTOM_MARGIN;
        ULONG newwidth = DEFAULT_WIDTH;
        ULONG newbuttonsstatus = TRUE;
        ULONG newslowmode = FALSE;

        DoMethod(obj,MUIM_GetConfigItem,CFGID_PEN_COLOR,(ULONG)&newpencolor);
        DoMethod(obj,MUIM_GetConfigItem,CFGID_PAPER_COLOR,(ULONG)&newpapercolor);
        DoMethod(obj,MUIM_GetConfigItem,CFGID_IS_TRANSPARENT,&newistransparent);
        DoMethod(obj,MUIM_GetConfigItem,CFGID_TOP_MARGIN,&newtopmargin);
        DoMethod(obj,MUIM_GetConfigItem,CFGID_BOTTOM_MARGIN,&newbottommargin);
        DoMethod(obj,MUIM_GetConfigItem,CFGID_WIDTH,&newwidth);
        DoMethod(obj,MUIM_GetConfigItem,CFGID_SLOW_MODE,&newslowmode);
        DoMethod(obj,MUIM_GetConfigItem,CFGID_BUTTONS_STATUS,&newbuttonsstatus);

        set(data->Equalizer,MUIA_Equalizer_Pen,MUI_ObtainPen(muiRenderInfo(obj),(struct MUI_PenSpec *)newpencolor,0));
        set(data->Equalizer,MUIA_Equalizer_Paper,MUI_ObtainPen(muiRenderInfo(obj),(struct MUI_PenSpec *)newpapercolor,0));
        set(data->Equalizer,MUIA_Equalizer_IsTransparent,newistransparent);
        set(data->Equalizer,MUIA_Equalizer_Width,newwidth);
        set(data->BtnGroup,MUIA_ShowMe,newbuttonsstatus);
        data->SlowMode = newslowmode;

        DoMethod(obj,MUIM_Group_InitChange);

        if(data->TopMargin)
        {
            DoMethod(data->EquGroup,MUIM_Group_Remove,data->TopMargin);
            DisposeObject(data->TopMargin);
        }
        if(data->BottomMargin)
        {
            DoMethod(data->EquGroup,MUIM_Group_Remove,data->BottomMargin);
            DisposeObject(data->BottomMargin);
        }

        if(newtopmargin == 0)
        {
            data->TopMargin = NULL;
        }
        else
        {
            data->TopMargin = VSpace(newtopmargin);
            DoMethod(data->EquGroup,MUIM_Group_AddHead,data->TopMargin);
        }

        if(newbottommargin == 0)
        {
            data->BottomMargin = NULL;
        }
        else
        {
            data->BottomMargin = VSpace(newbottommargin);
            DoMethod(data->EquGroup,MUIM_Group_AddTail,data->BottomMargin);
        }

        DoMethod(obj,MUIM_Group_ExitChange);
    }

    return rc;
}

static ULONG ScreamBar_Cleanup(struct IClass *cl,Object *obj,Msg msg)
{
    struct ScreamBarClassData *data = INST_DATA(cl,obj);
    ULONG pen, paper;

    get(data->Equalizer,MUIA_Equalizer_Pen,&pen);
    if(pen != -1UL) MUI_ReleasePen(muiRenderInfo(obj),pen);

    get(data->Equalizer,MUIA_Equalizer_Paper,&paper);
    if(paper != -1UL) MUI_ReleasePen(muiRenderInfo(obj),paper);

    return DoSuperMethodA(cl,obj,msg);
}

static ULONG ScreamBar_Get(struct IClass *cl,Object *obj,Msg msg)
{
	//struct ScreamBarClassData *data = INST_DATA(cl,obj);
	ULONG *store = ((struct opGet *)msg)->opg_Storage;

	switch(((struct opGet *)msg)->opg_AttrID)
	{
        /* Returns a CONST_STRPTR containing a displayable name of the sbar. This is optional. If missing,
        ** the class name will be used. You should generally localise this string. The result will be copied. */
        case MUIA_Screenbar_DisplayedName:
            *store = (ULONG)PROGRAM_NAME;
            return TRUE;
        /* Returns an image object that will be used to draw the image in the Settings window. This is exactly
        ** the same as implementing the MCC_Query(2) call in your MCC. The object will be disposed when it's
        ** no longer needed, no later than the last instance of the class is disposed */
        case MUIA_Screenbar_DisplayedImage:
            *store = (ULONG)RawimageObject,
                MUIA_Rawimage_Data, RawImg_Prefs,
                End;
            return TRUE;
    }

	return(DoSuperMethodA(cl,obj,msg));
}

static ULONG ScreamBar_AskMinMax(struct IClass *cl,Object *obj,struct MUIP_AskMinMax *msg)
{
    ULONG rc = DoSuperMethodA(cl,obj,msg);
    struct MUI_MinMax *mm = msg->MinMaxInfo;

    // mind that you should generally stick to fixed widths
    // the custom layouter isn't very smart :)
    //mm->MinWidth = mm->DefWidth = mm->MaxWidth = _screen(obj)->BarHeight * 2;

    // if you provide MinHeight that's bigger than the actual titlebar size,
    // your object will not be displayed in the titlebar!
    mm->MinHeight = mm->DefHeight = mm->MaxHeight = _screen(obj)->BarHeight + 1;

    return rc;
}

static ULONG ScreamBar_BuildSettingsPanel(UNUSED struct IClass *cl, UNUSED Object *obj, UNUSED struct MUIP_Screenbar_BuildSettingsPanel *msg)
{
    Object *LBL_PenColor,*LBL_PaperColor,*LBL_IsTransparent,*LBL_TopMargin,*LBL_BottomMargin,*LBL_Width,*LBL_SlowMode;
    Object *POP_PenColor,*POP_PaperColor,*CHK_IsTransparent,*NUM_TopMargin,*NUM_BottomMargin,*NUM_Width,*CHK_SlowMode;
    Object *LBL_ButtonsStatus;
    Object *CHK_ButtonsStatus;
    Object *GRP_Prefs;

    GRP_Prefs = MUI_NewObject(MUIC_Mccprefs,

        Child, ColGroup(2),
            GroupFrameT(GetString(MSG_SCOPE_TITLE_GRP)),

            Child, LBL_PenColor = Label2(GetString(MSG_SCOPE_COLOR_LBL)),
            Child, HGroup,
                Child, POP_PenColor = PoppenObject,
                    MUIA_CycleChain,TRUE,
                    MUIA_Penadjust_PSIMode,2,
                    MUIA_FixWidthTxt,"XXXX",
                    MUIA_ShortHelp, GetString(MSG_SCOPE_COLOR_HLP),
                    End,
                Child, HVSpace,
                End,

            Child, LBL_PaperColor = Label2(GetString(MSG_SCOPE_BACKGROUND_COLOR_LBL)),
            Child, HGroup,
                Child, POP_PaperColor = PoppenObject,
                    MUIA_CycleChain,TRUE,
                    MUIA_Penadjust_PSIMode,2,
                    MUIA_FixWidthTxt,"XXXX",
                    MUIA_ShortHelp, GetString(MSG_SCOPE_BACKGROUND_COLOR_HLP),
                    End,
                Child, HGroup,
                    MUIA_CycleChain, TRUE,
                    MUIA_ShortHelp, GetString(MSG_SCOPE_BACKGROUND_TRANSPARENT_HLP),
                    Child, CHK_IsTransparent = MUI_MakeObject(MUIO_Checkmark,GetString(MSG_SCOPE_BACKGROUND_TRANSPARENT_CHK)),
                    Child, LBL_IsTransparent = LLabel2(GetString(MSG_SCOPE_BACKGROUND_TRANSPARENT_CHK)),
                    Child, HVSpace,
                    End,
                End,

            Child, LBL_TopMargin = Label2(GetString(MSG_SCOPE_TOP_SPACING_LBL)),
            Child, NUM_TopMargin = SliderObject,
                MUIA_CycleChain, TRUE,
                MUIA_Numeric_Format, GetString(MSG_PIXELS_SLIDER_STRING_D_NUM),
                MUIA_Numeric_Min, 0,
                MUIA_Numeric_Max, 7,
                MUIA_Slider_Horiz, TRUE,
                MUIA_ShortHelp, GetString(MSG_SCOPE_TOP_SPACING_HLP),
                End,

            Child, LBL_BottomMargin = Label2(GetString(MSG_SCOPE_BOTTOM_SPACING_LBL)),
            Child, NUM_BottomMargin = SliderObject,
                MUIA_CycleChain, TRUE,
                MUIA_Numeric_Format, GetString(MSG_PIXELS_SLIDER_STRING_D_NUM),
                MUIA_Numeric_Min, 0,
                MUIA_Numeric_Max, 7,
                MUIA_Slider_Horiz, TRUE,
                MUIA_ShortHelp, GetString(MSG_SCOPE_BOTTOM_SPACING_HLP),
                End,

            Child, LBL_Width = Label2(GetString(MSG_SCOPE_WIDTH_STRING_D_NUM)),
            Child, NUM_Width = SliderObject,
                MUIA_CycleChain, TRUE,
                MUIA_Numeric_Format, GetString(MSG_PIXELS_SLIDER_STRING_D_NUM),
                MUIA_Numeric_Min, 10,
                MUIA_Numeric_Max, 100,
                MUIA_Slider_Horiz, TRUE,
                MUIA_ShortHelp, GetString(MSG_SCOPE_WIDTH_HLP),
                End,

            Child, HVSpace,
            Child, HGroup,
                Child, HGroup,
                    MUIA_CycleChain, TRUE,
                    MUIA_ShortHelp, GetString(MSG_SCOPE_SLOW_MODE_HLP),
                    Child, CHK_SlowMode = MUI_MakeObject(MUIO_Checkmark,GetString(MSG_SCOPE_SLOW_MODE_CHK)),
                    Child, LBL_SlowMode = LLabel2(GetString(MSG_SCOPE_SLOW_MODE_CHK)),
                    End,
                Child, HVSpace,
                End,

            End,

        Child, VGroup,
            GroupFrameT(GetString(MSG_BUTTONS_TITLE_GRP)),
            Child, HGroup,
                Child, HGroup,
                    MUIA_CycleChain, TRUE,
                    MUIA_ShortHelp, GetString(MSG_BUTTONS_SHOW_PLAYER_BUTTONS_HLP),
                    Child, CHK_ButtonsStatus = MUI_MakeObject(MUIO_Checkmark,GetString(MSG_BUTTONS_SHOW_PLAYER_BUTTONS_CHK)),
                    Child, LBL_ButtonsStatus = LLabel2(GetString(MSG_BUTTONS_SHOW_PLAYER_BUTTONS_CHK)),
                    End,
                Child, HVSpace,
                End,
            End,

        Child, HVSpace,

		Child, CrawlingObject,
			TextFrame,
			MUIA_FixHeightTxt, "\n",
			MUIA_Background, MUII_TextBack,
			MUIA_Virtgroup_Input, FALSE,
			Child, TextObject,
                MUIA_Text_PreParse, MUIX_C,
				MUIA_Text_Contents, MUIX_B "" PROGRAM_VERSION_SHORT "" MUIX_N "\n" PROGRAM_COPYRIGHT "\n",
				End,
			Child, TextObject,
                MUIA_Text_PreParse, MUIX_C,
				MUIA_Text_Contents, GetString(MSG_COPYRIGHT_MESSAGE_STR),
				End,
			Child, HyperlinkObject,
                MUIA_Text_SetMax, FALSE,
                MUIA_Text_PreParse, MUIX_C,
				MUIA_Text_Contents, "offset@cpcscene.net",
                MUIA_Hyperlink_URI, "mailto:offset@cpcscene.net",
				End,
			Child, TextObject,
                MUIA_Text_PreParse, MUIX_C,
				MUIA_Text_Contents, "\n" MUIX_B "" PROGRAM_VERSION_SHORT "" MUIX_N "\n" PROGRAM_COPYRIGHT,
				End,
            End,

        TAG_DONE, TAG_END);

	DoMethod(CHK_IsTransparent,MUIM_Notify,MUIA_Selected,MUIV_EveryTime,
    	 	 POP_PaperColor,3,MUIM_Set,MUIA_Disabled,MUIV_TriggerValue);

    DoMethod(GRP_Prefs,MUIM_Mccprefs_RegisterGadget,(ULONG)POP_PenColor     ,CFGID_PEN_COLOR     ,3,NULL,MUIA_Pendisplay_Spec,(ULONG)LBL_PenColor     );
    DoMethod(GRP_Prefs,MUIM_Mccprefs_RegisterGadget,(ULONG)POP_PaperColor   ,CFGID_PAPER_COLOR   ,3,NULL,MUIA_Pendisplay_Spec,(ULONG)LBL_PaperColor   );
    DoMethod(GRP_Prefs,MUIM_Mccprefs_RegisterGadget,(ULONG)CHK_IsTransparent,CFGID_IS_TRANSPARENT,3,NULL,MUIA_Selected       ,(ULONG)LBL_IsTransparent);
    DoMethod(GRP_Prefs,MUIM_Mccprefs_RegisterGadget,(ULONG)NUM_TopMargin    ,CFGID_TOP_MARGIN    ,3,NULL,MUIA_Numeric_Value  ,(ULONG)LBL_TopMargin    );
    DoMethod(GRP_Prefs,MUIM_Mccprefs_RegisterGadget,(ULONG)NUM_BottomMargin ,CFGID_BOTTOM_MARGIN ,3,NULL,MUIA_Numeric_Value  ,(ULONG)LBL_BottomMargin );
    DoMethod(GRP_Prefs,MUIM_Mccprefs_RegisterGadget,(ULONG)NUM_Width        ,CFGID_WIDTH         ,3,NULL,MUIA_Numeric_Value  ,(ULONG)LBL_Width        );
    DoMethod(GRP_Prefs,MUIM_Mccprefs_RegisterGadget,(ULONG)CHK_SlowMode     ,CFGID_SLOW_MODE     ,3,NULL,MUIA_Selected       ,(ULONG)LBL_SlowMode     );
    DoMethod(GRP_Prefs,MUIM_Mccprefs_RegisterGadget,(ULONG)CHK_ButtonsStatus,CFGID_BUTTONS_STATUS,3,NULL,MUIA_Selected       ,(ULONG)LBL_ButtonsStatus);

    return (ULONG)GRP_Prefs;
}

static ULONG ScreamBar_KnowsConfigItem(UNUSED struct IClass *cl, UNUSED Object *obj, UNUSED struct MUIP_Screenbar_KnowsConfigItem *msg)
{
    switch(msg->cfgid)
    {
        case CFGID_PEN_COLOR:
        case CFGID_PAPER_COLOR:
        case CFGID_IS_TRANSPARENT:
        case CFGID_TOP_MARGIN:
        case CFGID_BOTTOM_MARGIN:
        case CFGID_WIDTH:
        case CFGID_SLOW_MODE:
        case CFGID_BUTTONS_STATUS:
            return TRUE;
    }

    return FALSE;
}

static ULONG ScreamBar_DefaultConfigItem(UNUSED struct IClass *cl, UNUSED Object *obj, UNUSED struct MUIP_Screenbar_DefaultConfigItem *msg)
{
    switch(msg->cfgid)
    {
        case CFGID_PEN_COLOR     : return (ULONG)DEFAULT_PEN_COLOR;
        case CFGID_PAPER_COLOR   : return (ULONG)DEFAULT_PAPER_COLOR;
        case CFGID_IS_TRANSPARENT: return (ULONG)TRUE;
        case CFGID_TOP_MARGIN    : return DEFAULT_TOP_MARGIN;
        case CFGID_BOTTOM_MARGIN : return DEFAULT_BOTTOM_MARGIN;
        case CFGID_WIDTH         : return DEFAULT_WIDTH;
        case CFGID_SLOW_MODE     : return (ULONG)FALSE;
        case CFGID_BUTTONS_STATUS: return (LONG)TRUE;
    }

    return 0;
}

/*
    Use this macro to add your objects to the refresh list. This will ensure the configuration is reloaded.
    Flags: use 0 in case you only need to be redrawn, 1 in case you want to reload prefs values and refresh.
*/

#ifndef RedrawQ
/* for MUIM_UpdateConfig
 */
#define RedrawQ(m,o,flg) \
    do { \
    if ((((flg)==0) || ((flg)==1 && muiRenderInfo(o))) && (m->redrawcount < (LONG)(sizeof(m->redrawobj)/sizeof(m->redrawobj[0])))) \
    { \
        m->redrawflags[m->redrawcount] = flg; \
        m->redrawobj[m->redrawcount++] = o; \
    } } while(0)
#endif

static ULONG ScreamBar_UpdateConfigItem(UNUSED struct IClass *cl,Object *obj,struct MUIP_Screenbar_UpdateConfigItem *msg)
{
    switch(msg->cfgid)
    {
        case CFGID_PEN_COLOR:
        case CFGID_PAPER_COLOR:
        case CFGID_IS_TRANSPARENT:
        case CFGID_TOP_MARGIN:
        case CFGID_BOTTOM_MARGIN:
        case CFGID_WIDTH:
        case CFGID_SLOW_MODE:
        case CFGID_BUTTONS_STATUS:
            RedrawQ(msg,obj,1);
            break;
    }

    return 0;
}

static ULONG ScreamBar_Signal(struct IClass *cl, Object *obj, UNUSED struct MUIP_Screenbar_Signal *msg)
{
    struct ScreamBarClassData *data = INST_DATA(cl,obj);

    //D(bug("ScreamBar_Signal: %ld\n",data->AppData->LastOperationMode));

    // Toggle the play/pause button
    if(data->PlayDisplayed && data->AppData->LastOperationMode == 1)
    {
        data->PlayDisplayed = FALSE;
        set(data->PlayPause,MUIA_Group_ActivePage,1);
    }
    else if(!data->PlayDisplayed && data->AppData->LastOperationMode != 1)
    {
        data->PlayDisplayed = TRUE;
        set(data->PlayPause,MUIA_Group_ActivePage,0);
    }
    // Display the audio data
    if(!data->AppData->IsDisplayed)
    {
        ULONG isBehind;

        LockPubScreenList();
        GetAttr(SA_Behind,_screen(obj),(ULONG*) &isBehind);
        UnlockPubScreenList();

        if(!isBehind)
        {
            static ULONG count = 0;

            if(!data->SlowMode || count++%16 == 0)
            {
                MUI_Redraw(data->Equalizer,MADF_DRAWUPDATE);
                data->AppData->IsDisplayed = TRUE;
            }
        }
    }

    return 0;
}

DISPATCHERNAME_BEGIN(ScreamBar_Sbar)
    case OM_NEW                            : return ScreamBar_New               (cl,obj,(APTR)msg);
    case OM_DISPOSE                        : return ScreamBar_Dispose           (cl,obj,(APTR)msg);
    case OM_GET                            : return ScreamBar_Get               (cl,obj,(APTR)msg);
    case MUIM_Setup                        : return ScreamBar_Setup             (cl,obj,(APTR)msg);
    case MUIM_Cleanup                      : return ScreamBar_Cleanup           (cl,obj,(APTR)msg);
    case MUIM_AskMinMax                    : return ScreamBar_AskMinMax         (cl,obj,(APTR)msg);

    case MUIM_Screenbar_BuildSettingsPanel : return ScreamBar_BuildSettingsPanel(cl,obj,(APTR)msg);
    case MUIM_Screenbar_KnowsConfigItem    : return ScreamBar_KnowsConfigItem   (cl,obj,(APTR)msg);
    case MUIM_Screenbar_DefaultConfigItem  : return ScreamBar_DefaultConfigItem (cl,obj,(APTR)msg);
    case MUIM_Screenbar_UpdateConfigItem   : return ScreamBar_UpdateConfigItem  (cl,obj,(APTR)msg);
    case MUIM_Screenbar_Signal             : return ScreamBar_Signal            (cl,obj,(APTR)msg);
DISPATCHER_END
DISPATCHER_GATE(ScreamBar_Sbar)

/* /// */

/* /// "Main application" */

int main(int argc, char **argv)
{
    int res;
    BOOL wasAlreadyLaunched;
    struct WBStartup *wb_startup;
    struct { CONST_STRPTR fileName; } params = { NULL };
    static struct SmartArgs args;
    struct PluginMessage *PluginMsg;
    struct PluginMessage *ReplyMsg;

    res = RETURN_OK;

    // First of all we need the locale to be able to display something in the proper language
    if(!LocaleOpen(PROGRAM_CATALOG,PROGRAM_VER,PROGRAM_REV))
    {
        ShowRequester(MUIX_C"Could not open locale!\n", "  Abort  ");
        return RETURN_WARN;
    }

    // Check for MagicBeacon library
    if(!(MagicBeaconBase = OpenLibrary(MAGICBEACON_NAME, 0L)))
    {
        ShowRequester(MUIX_C"Could not open "MAGICBEACON_NAME"!\n", "  Abort  ");
        res = RETURN_WARN;
        goto out;
    }

    // Check run mode
    if(argc)
        wb_startup = NULL;
    else
        wb_startup = (struct WBStartup *)argv;

    // Prepare the SmartArgs structure
    memset(&args, 0, sizeof(args));
    args.sa_Template = "URL=FILE";
    args.sa_Parameter = (IPTR *)&params;
    args.sa_FileParameter = 1;
    args.sa_Window = "CON:////"PROGRAM_NAME"/AUTO/CLOSE/WAIT";

    // Parse the arguments (CLI arguments or Workbench tooltypes)
    res = SmartReadArgs(wb_startup, &args);

    if(res != RETURN_OK)
    {
        ShowRequester(GetString(MSG_WRONG_PARAMETERS_REQ), GetString(MSG_ABORT_BTN));
        goto out;
    }

    // Quickly check if a plugin capable instance of AmiNetRadio or AmigaAMP is running or could be run
    if(!FindPort("AmigaAMP plugin port") || !FindPort("AMINETRADIO.1"))
    {
        BOOL launched = FALSE;
        BPTR progDir = Lock("PROGDIR:", SHARED_LOCK);

        wasAlreadyLaunched = FALSE;

        // Try to launch AmiNetRadio
        if(progDir)
        {
            BPTR oldDir = CurrentDir(progDir);
            BPTR anrLock = Lock("/AmiNetRadio", SHARED_LOCK);

            if(anrLock)
            {
                if(Execute("C:WBRun /AmiNetRadio", (BPTR)0, (BPTR)0))
                {
                    LONG c = 100;

                    while(c--)
                    {
                        if(FindPort("AmigaAMP plugin port") && FindPort("AMINETRADIO.1"))
                        {
                            launched = TRUE;
                            break;
                        }
                        Delay(5);
                    }
                }
                UnLock(anrLock);
            }
            else
            {
                ShowRequester(GetString(MSG_AMINETRADIO_NOT_FOUND_REQ), GetString(MSG_ABORT_BTN));
                res = RETURN_WARN;
                goto out;
            }

            CurrentDir(oldDir);
            UnLock(progDir);
        }

        // Do we finally have AmiNetRadio running?
        if(!launched)
        {
            ShowRequester(GetString(MSG_AMINETRADIO_NOT_LAUNCHED_REQ), GetString(MSG_ABORT_BTN));
            res = RETURN_WARN;
            goto out;
        }
    }
    else
    {
        wasAlreadyLaunched = TRUE;
    }

    // If any was provided, pass file name to AmiNetRadio
    if(params.fileName)
    {
        CONST_STRPTR rxPlayTemplate =  "PLAY NAME \"%s\"";
        STRPTR rxString = AllocVec(strlen(rxPlayTemplate) + strlen(params.fileName), MEMF_ANY);

        if(rxString)
        {
            struct MsgPort *replyPort;
            struct RexxMsg *rxMsg;

            if((replyPort = CreateMsgPort()))
            {
                if((rxMsg = CreateRexxMsg(replyPort,NULL,NULL)))
                {
                    NewRawDoFmt(rxPlayTemplate, RAWFMTFUNC_STRING, rxString, params.fileName);
                    SendRxMsgToANR(rxString, rxMsg, replyPort);
                    DeleteRexxMsg(rxMsg);
                }
                DeleteMsgPort(replyPort);
            }
            //kprintf("[ScreamBar] Send AREXX command to AmiNetRadio: %s\n", rxString);
            FreeVec(rxString);
        }
    }
    // If launched without file and already launched, then bring it to front!
    else if(wasAlreadyLaunched)
    {
        struct MsgPort *replyPort;
        struct RexxMsg *rxMsg;

        if((replyPort = CreateMsgPort()))
        {
            if((rxMsg = CreateRexxMsg(replyPort,NULL,NULL)))
            {
                SendRxMsgToANR("HIDE", rxMsg, replyPort);
                SendRxMsgToANR("SHOW", rxMsg, replyPort);
                DeleteRexxMsg(rxMsg);
            }
            DeleteMsgPort(replyPort);
        }
    }

    // Allocate all user resources
    if(PluginInit())
    {
        ULONG InfoSignal;
        ULONG PluginSignal;

        // Allocate a sigbit for receiving signals FROM AmigaAMP
        PluginTask   = (struct Process *)FindTask(NULL);
		PluginSignal = AllocSignal(-1);
		InfoSignal   = AllocSignal(-1);

        if(PluginSignal != -1UL && InfoSignal != -1UL)
        { 
            PluginMask  = 1L << PluginSignal;
            InfoMask    = 1L << InfoSignal;

            // Allocate a message and reply port for sending messages TO AmigaAMP
            PluginMsg = AllocVec(sizeof(struct PluginMessage), MEMF_PUBLIC|MEMF_CLEAR);
            PluginRP = CreatePort(0,0);

            if(PluginMsg && PluginRP)
            {
                // Tell AmigaAMP all the details it needs to know
                PluginMsg->msg.mn_Node.ln_Type = NT_MESSAGE;
                PluginMsg->msg.mn_Length       = sizeof(struct PluginMessage);
                PluginMsg->msg.mn_ReplyPort    = PluginRP;
                PluginMsg->PluginMask          = PluginMask;
                PluginMsg->PluginTask          = PluginTask;
                PluginMsg->SpecRawL            = &SpecRawL;
                PluginMsg->SpecRawR            = &SpecRawR;
                PluginMsg->InfoMask            = InfoMask;   // v1.2
                PluginMsg->tinfo               = &tinfo;     // v1.2
                PluginMsg->PluginWP            = NULL;       // v1.4
                PluginMsg->SampleRaw           = &SampleRaw; // v1.3

                Forbid();

                if((PluginMP = FindPort("AmigaAMP plugin port")))
                {
                    PutMsg(PluginMP, (struct Message *)PluginMsg);
                    Permit();
                    // Wait for a reply
                    WaitPort(PluginRP);
                    // Let's see if AmigaAMP accepted our registration attempt
                    if((ReplyMsg = (struct PluginMessage *)GetMsg(PluginRP)))
                        Accepted = ReplyMsg->Accepted;
                    else
                        Accepted = FALSE;

                    if(Accepted)
                    {
                        /* If it did, start the plugin loop */
                        PluginLoop();

                        /* Tell AmigaAMP that this plugin is going down */
                        PluginMsg->PluginMask          = 0;
                        PluginMsg->PluginTask          = NULL;
                        PluginMsg->SpecRawL            = NULL;
                        PluginMsg->SpecRawR            = NULL;
                        PluginMsg->InfoMask            = 0;     // v1.2
                        PluginMsg->tinfo               = NULL;  // v1.2
                        PluginMsg->PluginWP            = NULL;  // v1.4
                        PluginMsg->SampleRaw           = NULL;  // v1.3

                        PutMsg(PluginMP, (struct Message *)PluginMsg);
                        // Wait for confirmation before going on!
                        WaitPort(PluginRP);
                        GetMsg(PluginRP);
                        // Now that AmigaAMP knows that we're gone, we can quit
                    }
                    else
                    {
                        // If AmigaAMP didn't accept us, tell the user about it
                        ShowRequester(GetString(MSG_PLUGIN_REJECTED_REQ), GetString(MSG_ABORT_BTN));
                        res = RETURN_WARN;
                    }
                }
                else
                {
                    Permit();
                    ShowRequester(GetString(MSG_AMINETRADIO_NOT_RUNNING_REQ), GetString(MSG_ABORT_BTN));
                    res = RETURN_WARN;
                }
            }
            else
            {
                ShowRequester(GetString(MSG_CANNOT_CREATE_MESSAGE_PORT_REQ), GetString(MSG_ABORT_BTN));
                res = RETURN_WARN;
            }

            // Free all resources
            if(PluginMsg) FreeVec(PluginMsg);
            if(PluginRP) DeletePort(PluginRP);
        }
        else
        {
            ShowRequester(GetString(MSG_SIGNAL_ALLOCATION_FAILURE_REQ), GetString(MSG_ABORT_BTN));
            res = RETURN_WARN;
        }
		if(PluginSignal != -1UL) FreeSignal(PluginSignal);
		if(InfoSignal   != -1UL) FreeSignal(InfoSignal);
    }
    else
    {
        //kprintf("[ScreamBar] Plugin not started (already running?)\n");
        //ShowRequester(GetString(MSG_PLUGIN_INITIALIZATION_FAILED_REQ), GetString(MSG_ABORT_BTN));
        //res = RETURN_WARN;
    }

    // Free all user resources
    PluginExit();

    // Free smart arguments structures
    SmartFreeArgs(&args);

    // We shall also close MagicBeacon (if it was used)
    CloseLibrary(MagicBeaconBase);
out:
    // We do not need locale anymore!
    LocaleClose();

    return res;
}

/* /// */

/* /// "Visual plug-in interface" */

/****************************************************************************/
/* This function will be called once when the plugin is started. You should */
/* allocate all needed resources here. Return TRUE if all went well.        */
/****************************************************************************/

static WORD PluginInit(void)
{
    Object *Hide, *Quit;

    // Application stuff
    AppData = AllocVec(sizeof(struct ApplicationData), MEMF_PUBLIC|MEMF_CLEAR);
    // Own MCCs
    ApplicationClass = MUI_CreateCustomClass(NULL,MUIC_Application ,NULL,sizeof(struct ApplicationClassData),DISPATCHER_REF(MCC_Application));
    EqualizerClass = MUI_CreateCustomClass(NULL,MUIC_Area ,NULL,sizeof(struct EqualizerClassData),DISPATCHER_REF(MCC_Equalizer));
    // Screenbar class
    ScreenbarClass = MUI_CreateCustomClass(NULL,MUIC_Group,NULL,sizeof(struct ScreamBarClassData),DISPATCHER_REF(ScreamBar_Sbar));

    if(AppData && ScreenbarClass && EqualizerClass)
    {
        Object *MyGroup;

        AppData->MyApp = NewObject(ApplicationClass->mcc_Class,NULL,
    		MUIA_Application_Title         , PROGRAM_NAME,
    		MUIA_Application_Version       , PROGRAM_VERSION,
    		MUIA_Application_Copyright     , PROGRAM_COPYRIGHT,
    		MUIA_Application_Author        , PROGRAM_AUTHOR,
    		MUIA_Application_Description   , GetString(MSG_DESCRIPTION_STR),
            MUIA_Application_DiskObject    , GetDiskObject(PROGRAM_EXE),
            MUIA_Application_UseCommodities, FALSE,
            MUIA_Application_UseRexx       , FALSE,
            MUIA_Application_SingleTask    , TRUE,

			SubWindow, AppData->MyWin = WindowObject,
				MUIA_Window_Title, PROGRAM_NAME,
                MUIA_Window_ScreenTitle, PROGRAM_NAME " " VERSION_STRING,
                //MUIA_Window_ID, MAKE_ID('S','C','M','B'),
                //MUIA_Window_SizeGadget, FALSE,
				WindowContents, VGroup,
                    MUIA_Background, MUII_RequesterBack,
                    Child, HGroup,
                        Child, VGroup,
                            Child, RawimageObject,
                                MUIA_Rawimage_Data, RawImg_Logo,
                                End,
                            Child, MyGroup = VGroup,
                                InnerSpacing(10,0),
                                End,
                            End,
                        Child, ColGroup(2),
                            TextFrame,
                            MUIA_Background, MUII_TextBack,
                            Child, Label(GetString(MSG_MEDIA_STREAM_STR)),
                            Child, HGroup,
                                Child, AppData->Stream = HyperlinkObject,
                                    MUIA_Text_PreParse, MUIX_L,
                                    MUIA_Text_SetMax, FALSE, // Does not work!
                                    MUIA_Hyperlink_URI, GetString(MSG_NO_URL_STR),
                                    End,
                                Child, HVSpace, // Work around for MUIA_Text_SetMax not working...
                                End,

                            Child, Label(GetString(MSG_MEDIA_GENRE_STR)),
                            Child, AppData->Genre = TextObject, MUIA_Text_Contents, "-", End,

                            Child, MUI_MakeObject(MUIO_HBar,2),
                            Child, MUI_MakeObject(MUIO_HBar,2),

                            Child, Label(GetString(MSG_MEDIA_TITLE_STR)),
                            Child, AppData->Title = TextObject, MUIA_Text_Contents, "-", End,

                            Child, Label(GetString(MSG_MEDIA_ARTIST_STR)),
                            Child, AppData->Artist = TextObject, MUIA_Text_Contents, "-", End,

                            Child, Label(GetString(MSG_MEDIA_ALBUM_STR)),
                            Child, AppData->Album = TextObject, MUIA_Text_Contents, "-", End,

                            Child, Label(GetString(MSG_MEDIA_YEAR_STR)),
                            Child, AppData->Year = TextObject, MUIA_Text_Contents, "-", End,

                            End,
                        End,
                    Child, HGroup,
                        MUIA_Group_SameWidth, TRUE,
    					Child, Hide = MakeButton(GetString(MSG_OKAY_BTN), GetString(MSG_OKAY_HLP)),
                        Child, HVSpace,
                        Child, HVSpace,
    					Child, Quit = MakeButton(GetString(MSG_QUIT_BTN), GetString(MSG_QUIT_HLP)),
    					End,
                    End,
				End,
			TAG_DONE);

        if(AppData->MyApp
        && (AppData->BeaconHandle = MagicBeacon_ApplicationRegisterTags(
            MAGICBEACON_APPLICATIONNAME, (IPTR)PROGRAM_NAME,
            TAG_END))
        && (AppData->ANR_ReplyPort = CreateMsgPort())
        && (AppData->ANR_RxMsg = CreateRexxMsg(AppData->ANR_ReplyPort,NULL,NULL)))
        {
            struct Screen *screen;
            struct Task *psm;

            DoMethod(AppData->MyWin,MUIM_Notify,MUIA_Window_CloseRequest,TRUE,
                     AppData->MyWin,3,MUIM_Set,MUIA_Window_Open,FALSE);

            DoMethod(Hide,MUIM_Notify,MUIA_Pressed,FALSE,
                     AppData->MyWin,3,MUIM_Set,MUIA_Window_Open,FALSE);
            DoMethod(Quit,MUIM_Notify,MUIA_Pressed,FALSE,
                     AppData->MyApp,2,MUIM_Application_ReturnID,MUIV_Application_ReturnID_Quit);

            if((AppData->Equalizer = NewObject(EqualizerClass->mcc_Class,NULL,
                                MUIA_Equalizer_AudioData, (ULONG)AppData->AudioData,
                                TAG_DONE)))
            {
                DoMethod(MyGroup, OM_ADDMEMBER, AppData->Equalizer);
            }

            RESET_STRING(AppData->DisplayMessage,GetString(MSG_NOT_PLAYING_STR));

            // We MUST set the name of our class!
            ScreenbarClass->mcc_Class->cl_ID = PROGRAM_NAME".sbar";

            // Give our instances a way to call us back
            ScreenbarClass->mcc_Class->cl_UserData = (ULONG)AppData;

            // Install the plugin!
            ScreenbarControl(SBCT_InstallPlugin,(ULONG)ScreenbarClass,TAG_DONE);

            // Get the common signal for screen bars
            screen = LockPubScreen(NULL);
            GetAttr(SA_ScreenbarSignal, screen, &AppData->MySignal);
            UnlockPubScreen(NULL, screen);

            // Get the Public Screen Manager Task's ID
            Forbid();
            psm = FindTask("� Public Screen Manager �");
            if(psm)
                AppData->PSMID = psm->tc_ETask->UniqueID;
            Permit();

            return (psm != NULL);
        }
    }

    return FALSE;
}

/******************************************************************************/
/* This function will be called when the plugin is shut down. You should free */
/* all previously allocated resources here.                                   */
/******************************************************************************/

static VOID PluginExit(void)
{
    // MUST uninstall the module before exiting!!!
    if(AppData)
    {
        if(AppData->MyApp)
        {
            ScreenbarControl(SBCT_UninstallPlugin,(ULONG)ScreenbarClass,TAG_DONE);
            MUI_DisposeObject(AppData->MyApp);
        }

        if(AppData->ANR_RxMsg) DeleteRexxMsg(AppData->ANR_RxMsg);
        if(AppData->ANR_ReplyPort) DeleteMsgPort(AppData->ANR_ReplyPort);
        if(AppData->BeaconHandle) MagicBeacon_ApplicationUnregister(AppData->BeaconHandle);

        if(ScreenbarClass) MUI_DeleteCustomClass(ScreenbarClass);
        if(EqualizerClass) MUI_DeleteCustomClass(EqualizerClass);

        FreeVec(AppData);
    }
}

/*******************************************************************************/
/* This is the main Plugin Loop. It will receive a signal matching PluginMask  */
/* each time new spectral data is ready. The data is stored in two arrays,     */
/* UWORD SpecRawL[512] and UWORD SpecRawR[512]. The scale is logarithmic, i.e. */
/* 0 means below -96dB, 65535 means 0dB. Another array SampleRaw[1024] holds   */
/* the current waveform. This array is not divided into left and right channel */
/* but contains interleaved stereo data, 16-bit signed just like in an AIFF or */
/* in a CDDA file.                                                             */
/* No matter how long it takes until your plugin actually processes the data,  */
/* the memory referenced by the array pointers always remains valid!           */
/*                                                                             */
/* Your plugin loop MUST quit when it receives SIGBREAKF_CTRL_C. If you've     */
/* opened a window you should react to the close gadget as well. For full      */
/* screen plugins I strongly recommend checking the ESC key.                   */
/*******************************************************************************/

static VOID PluginLoop(void)
{
    ULONG Signals;
    ULONG ID;
    STRPTR streamGenre = NULL;
    STRPTR streamName = NULL;
    STRPTR streamURI = NULL;
    BOOL isFile;
    BOOL upToDate = FALSE;

    while((ID = DoMethod(AppData->MyApp,MUIM_Application_NewInput,&Signals)) != (ULONG)MUIV_Application_ReturnID_Quit)
    {
        if(ID == RETURNID_OPENWINDOW)
        {
            ULONG icon, open;
            struct Screen *winScreen = NULL;
            struct Screen *curScreen = LockPubScreen(NULL);

            // Uniconify if iconified
            get(AppData->MyApp,MUIA_Application_Iconified,&icon);
            if(icon) set(AppData->MyApp,MUIA_Application_Iconified,FALSE);
            // Get Window screen if already opened
            get(AppData->MyWin,MUIA_Window_Open,&open);
            if(open) get(AppData->MyWin,MUIA_Window_Screen,&winScreen);
            // Close if opened on a different screen
            if(open && winScreen != curScreen) set(AppData->MyWin,MUIA_Window_Open,FALSE);
            // Open the window anycase
            set(AppData->MyWin,MUIA_Window_Open,TRUE);

            UnlockPubScreen(NULL, curScreen);
        }

        // Is there something to do?
        if(Signals)
        {
            Signals = Wait(Signals | SIGBREAKF_CTRL_C | PluginMask | InfoMask);

            // Break received -> quit at once!
            if(Signals & SIGBREAKF_CTRL_C) break;

            // New data or track info has arrived!
            if(Signals & (PluginMask | InfoMask)) 
            {
                // Merged data and track info because of weird ANR behaviour...
                // ANR seems to never send the stop operation mode through
                // InfoMask, then we have to catch it through PluginMask!
                // Moreover, with some player, it will not use the InfoMask
                // at all so that we must check both flags and do all the job here...

                /********************************** PluginMask **********************************/
                /* Visualize SpecRawL[0..511], SpecRawR[0..511] and/or SampleRaw[0..1023] here! */
                /********************************************************************************/

                /****************************** InfoMask ******************************/
                /* If you want to display anything from struct TrackInfo, do it here! */
                /**********************************************************************/

                // Check operation mode or track change
                if(AppData->LastOperationMode != tinfo->DriveMode
                || (Signals & InfoMask))
                {
                    //kprintf("[ScreamBar] Update track!\n");

                    AppData->LastOperationMode = tinfo->DriveMode;

                    if(tinfo->DriveMode == 0) // Stop?
                    {
                        RESET_STRING(AppData->DisplayMessage,GetString(MSG_NOT_PLAYING_STR));
                        set(AppData->Stream, MUIA_Text_Contents, "-");
                        set(AppData->Stream, MUIA_Hyperlink_URI, GetString(MSG_NO_URL_STR));
                        set(AppData->Title , MUIA_Text_Contents, "-");
                        set(AppData->Artist, MUIA_Text_Contents, "-");
                        set(AppData->Album , MUIA_Text_Contents, "-");
                        set(AppData->Year  , MUIA_Text_Contents, "-");
                        set(AppData->Genre , MUIA_Text_Contents, "-");
                        upToDate = FALSE;
                    }
                    else if(tinfo->DriveMode == 2) // Pause?
                    {
                        upToDate = FALSE;
                    }
                    else
                    {
                        streamURI = SendRxMsgToANR("SITEURL",
                                                   AppData->ANR_RxMsg,
                                                   AppData->ANR_ReplyPort);
                        // AmiNetRadio sends back its URL when reading a local file
                        if((isFile = (strcmp(streamURI,"http://anr.amigazeux.net") == 0)))
                        {
                            if((streamURI = SendRxMsgToANR("STREAMURL",
                                                           AppData->ANR_RxMsg,
                                                           AppData->ANR_ReplyPort)))
                                streamName = FilePart(streamURI);
                            else
                                streamName = NULL;

                            streamGenre = NULL;
                        }
                        else
                        {
                            streamName = SendRxMsgToANR("TITLE",
                                                        AppData->ANR_RxMsg,
                                                        AppData->ANR_ReplyPort);
                            streamGenre = SendRxMsgToANR("GENRE",
                                                        AppData->ANR_RxMsg,
                                                        AppData->ANR_ReplyPort);
                        }

                        #define CHECK_STRING(string, default) ((string && *string != '\0') ? string : default)

                        STRPTR stream  = CHECK_STRING(streamName              , GetString(MSG_UNKNOWN_NAME_STR   ));
                        STRPTR title   = CHECK_STRING(CHECK_STRING((STRPTR)tinfo->ID3title, streamName)
                                                                              , GetString(MSG_UNKNOWN_TITLE_STR  ));
                        STRPTR artist  = CHECK_STRING((STRPTR)tinfo->ID3artist, GetString(MSG_UNKNOWN_ARTIST_STR ));
                        STRPTR album   = CHECK_STRING((STRPTR)tinfo->ID3album , GetString(MSG_UNKNOWN_ALBUM_STR  ));
                        STRPTR year    = CHECK_STRING((STRPTR)tinfo->ID3year  , GetString(MSG_UNKNOWN_YEAR_STR   ));
                        STRPTR genre   = CHECK_STRING((STRPTR)tinfo->ID3genre , CHECK_STRING(streamGenre,
                                                                                GetString(MSG_UNKNOWN_GENRE_STR  )));
                        // Send a notification to MagicBeacon on track name change (or play after pause/stop)
                        if(!upToDate
                        || strcmp(AppData->LastTrackTitle , title )
                        || strcmp(AppData->LastTrackStream, stream)
                        || strcmp(AppData->LastTrackArtist, artist)
                        || strcmp(AppData->LastTrackAlbum , album )
                        || strcmp(AppData->LastTrackYear  , year  )
                        || strcmp(AppData->LastTrackGenre , genre ))
                        {
                            LONG len;

                            struct TagItem taglist[2] =
                            {
                                { CST_GetUnterminatedBytes, (ULONG)&len },
                                { TAG_END, TAG_DONE }
                            };

                            UBYTE localTitle[MAX_FIELD_SIZE];
                            UBYTE localStream[MAX_FIELD_SIZE];
                            UBYTE localArtist[MAX_FIELD_SIZE];
                            UBYTE localAlbum[MAX_FIELD_SIZE];
                            UBYTE localYear[MAX_FIELD_SIZE];
                            UBYTE localGenre[MAX_FIELD_SIZE];

                            ULONG lenStream = strlen(stream);
                            ULONG lenTitle  = strlen(title);
                            ULONG lenArtist = strlen(artist);
                            ULONG lenAlbum  = strlen(album);
                            ULONG lenYear   = strlen(year);
                            ULONG lenGenre  = strlen(genre);

                            ULONG lenLocalStream;
                            ULONG lenLocalTitle;
                            ULONG lenLocalArtist;
                            ULONG lenLocalAlbum;
                            ULONG lenLocalYear;
                            ULONG lenLocalGenre;

                            STRPTR utf8Title = NULL;

                            CONST_STRPTR msgTemplate = "%s %s\n"
                                                       "%s %s\n"
                                                       "\n"
                                                       "%s %s\n"
                                                       "%s %s\n"
                                                       "%s %s\n"
                                                       "%s %s";

                            ULONG lenMessage =  strlen(msgTemplate)
                                             + lenStream
                                             + lenGenre
                                             + lenTitle
                                             + lenArtist
                                             + lenAlbum
                                             + lenYear;

                            if(lenStream > MAX_FIELD_SIZE-1) lenStream = MAX_FIELD_SIZE-1;
                            if(lenTitle  > MAX_FIELD_SIZE-1) lenTitle  = MAX_FIELD_SIZE-1;
                            if(lenArtist > MAX_FIELD_SIZE-1) lenArtist = MAX_FIELD_SIZE-1;
                            if(lenAlbum  > MAX_FIELD_SIZE-1) lenAlbum  = MAX_FIELD_SIZE-1;
                            if(lenYear   > MAX_FIELD_SIZE-1) lenYear   = MAX_FIELD_SIZE-1;
                            if(lenGenre  > MAX_FIELD_SIZE-1) lenGenre  = MAX_FIELD_SIZE-1;

                            // Store last song info
                            CopyMem(stream, AppData->LastTrackStream, 1 + lenStream);
                            CopyMem(title , AppData->LastTrackTitle , 1 + lenTitle);
                            CopyMem(artist, AppData->LastTrackArtist, 1 + lenArtist);
                            CopyMem(album , AppData->LastTrackAlbum , 1 + lenAlbum);
                            CopyMem(year  , AppData->LastTrackYear  , 1 + lenYear);
                            CopyMem(genre , AppData->LastTrackGenre , 1 + lenGenre);

                            // Check for UTF-8 source
                            lenLocalStream = ConvertTagList(stream,MAX_FIELD_SIZE,localStream,MAX_FIELD_SIZE,MIBENUM_UTF_8,MIBENUM_SYSTEM,taglist);
                            lenLocalTitle  = ConvertTagList(title ,MAX_FIELD_SIZE,localTitle ,MAX_FIELD_SIZE,MIBENUM_UTF_8,MIBENUM_SYSTEM,taglist);
                            lenLocalArtist = ConvertTagList(artist,MAX_FIELD_SIZE,localArtist,MAX_FIELD_SIZE,MIBENUM_UTF_8,MIBENUM_SYSTEM,taglist);
                            lenLocalAlbum  = ConvertTagList(album ,MAX_FIELD_SIZE,localAlbum ,MAX_FIELD_SIZE,MIBENUM_UTF_8,MIBENUM_SYSTEM,taglist);
                            lenLocalYear   = ConvertTagList(year  ,MAX_FIELD_SIZE,localYear  ,MAX_FIELD_SIZE,MIBENUM_UTF_8,MIBENUM_SYSTEM,taglist);
                            lenLocalGenre  = ConvertTagList(genre ,MAX_FIELD_SIZE,localGenre ,MAX_FIELD_SIZE,MIBENUM_UTF_8,MIBENUM_SYSTEM,taglist);

                            if(lenLocalStream > 0 && lenStream != lenLocalStream) { stream = localStream; kprintf("[ScreamBar] UTF-8 Stream detected!\n"); }
                            if(lenLocalTitle  > 0 && lenTitle  != lenLocalTitle ) { title  = localTitle ; kprintf("[ScreamBar] UTF-8 Title detected!\n" ); utf8Title = title; }
                            if(lenLocalArtist > 0 && lenArtist != lenLocalArtist) { artist = localArtist; kprintf("[ScreamBar] UTF-8 Artist detected!\n"); }
                            if(lenLocalAlbum  > 0 && lenAlbum  != lenLocalAlbum ) { album  = localAlbum ; kprintf("[ScreamBar] UTF-8 Album detected!\n" ); }
                            if(lenLocalYear   > 0 && lenYear   != lenLocalYear  ) { year   = localYear  ; kprintf("[ScreamBar] UTF-8 Year detected!\n"  ); }
                            if(lenLocalGenre  > 0 && lenGenre  != lenLocalGenre ) { genre  = localGenre ; kprintf("[ScreamBar] UTF-8 Genre detected!\n" ); }

                            // Update main window
                            set(AppData->Stream, MUIA_Text_Contents, stream);
                            set(AppData->Title , MUIA_Text_Contents, title);
                            set(AppData->Artist, MUIA_Text_Contents, artist);
                            set(AppData->Album , MUIA_Text_Contents, album);
                            set(AppData->Year  , MUIA_Text_Contents, year);
                            set(AppData->Genre , MUIA_Text_Contents, genre);

                            if(isFile)
                            {
                                #define AMBIENT_URI "Ambient:"
                                #define AMBIENT_MODE "?view=list&mode=all" // Does not work? (neither does &top=0&left=0&width=100&height=100)

                                STRPTR localURI;
                                ULONG pathLen = PathPart(streamURI) - streamURI;

                                if(pathLen && (localURI = AllocVec(strlen(AMBIENT_URI) + pathLen + strlen(AMBIENT_MODE) + 1, MEMF_ANY)))
                                {
                                    CopyMem(AMBIENT_URI, localURI, strlen(AMBIENT_URI));
                                    CopyMem(streamURI, localURI + strlen(AMBIENT_URI), pathLen);
                                    CopyMem(AMBIENT_MODE, localURI + strlen(AMBIENT_URI) + pathLen, strlen(AMBIENT_MODE));
                                    *(localURI + strlen(AMBIENT_URI) + pathLen + strlen(AMBIENT_MODE)) = '\0';

                                    //kprintf("%s\n", localURI);
                                    set(AppData->Stream, MUIA_Hyperlink_URI, localURI);

                                    FreeVec(localURI);
                                }
                                else
                                {
                                    set(AppData->Stream, MUIA_Hyperlink_URI, GetString(MSG_NO_URL_STR) );
                                }
                            }
                            else
                            {
                                if(streamURI && *streamURI != '\0')
                                    set(AppData->Stream, MUIA_Hyperlink_URI, streamURI);
                                else
                                    set(AppData->Stream, MUIA_Hyperlink_URI, GetString(MSG_NO_URL_STR) );
                            }

                            // Update short help (if possible)
                            if(lenMessage < MAX_MESSAGE_SIZE)
                            {
                                NewRawDoFmt(msgTemplate, RAWFMTFUNC_STRING, AppData->DisplayMessage,
                                            GetString(MSG_MEDIA_STREAM_STR), stream,
                                            GetString(MSG_MEDIA_GENRE_STR) , genre,
                                            GetString(MSG_MEDIA_TITLE_STR) , title,
                                            GetString(MSG_MEDIA_ARTIST_STR), artist,
                                            GetString(MSG_MEDIA_ALBUM_STR) , album,
                                            GetString(MSG_MEDIA_YEAR_STR)  , year);
                            }
                            else
                            {
                                kprintf("[ScreamBar] Song information too long!\n");
                                *AppData->DisplayMessage = '\0';
                            }

                            // Prepare the message for Magic Beacon
                            if(AppData->BeaconHandle)
                            {
                                MagicBeacon_BeaconSendTags(AppData->BeaconHandle,
                                    MAGICBEACON_NOTIFICATIONNAME        , (IPTR)MBN_TRACKNEXT,
                                    utf8Title ? MAGICBEACON_MESSAGE_UTF8
                                              : MAGICBEACON_MESSAGE     , (IPTR)(utf8Title ? utf8Title : title),
                                    TAG_END);
                    		}
                            else
                            {
                                kprintf("[ScreamBar] MagicBeacon not found: ignoring notification.\n");
                    		}

                            // Done!
                            upToDate = TRUE;
                        }
                    }
                }

                // Copy data to display into our private buffer
                if(Signals & PluginMask)
                {
                    CopyMem(SampleRaw, AppData->AudioData, AUDIO_DATA_SIZE*sizeof(WORD));
                    AppData->IsDisplayed = FALSE;

                    // Signal requested!
                    ULONG value;
                    struct Task *psm;

                    // Notify the screen bars
                    Forbid();
                    psm = FindTaskByPID(AppData->PSMID);
                    if(psm)
                        Signal(psm, 1 << AppData->MySignal);
                    Permit();

                    // Notify the window if opened
                    get(AppData->MyWin, MUIA_Window_Open, &value);
                    if(value) MUI_Redraw(AppData->Equalizer,MADF_DRAWUPDATE);
                }
            }
        }
    }

    set(AppData->MyWin,MUIA_Window_Open,FALSE);
}

/* /// */

/* /// "Tool functions" */

static Object * MakeButton(CONST_STRPTR label, CONST_STRPTR help)
{
    Object *obj = SimpleButton(label);

    SetAttrs(obj,
        MUIA_ShortHelp, help,
        MUIA_CycleChain, TRUE,
        TAG_DONE);

    return obj;
}

static VOID ShowRequester(char *Text, char *Button)
{
    struct EasyStruct Req;

    Req.es_Title        = PROGRAM_NAME;
    Req.es_TextFormat   = (UBYTE *)Text;
    Req.es_GadgetFormat = (UBYTE *)Button;
    EasyRequestArgs(NULL, &Req, NULL, NULL);
}

static STRPTR SendRxMsgToANR(STRPTR dataName, struct RexxMsg *rxMsg, struct MsgPort *replyPort)
{
    rxMsg->rm_Args[0] = dataName;
    rxMsg->rm_Action  = RXCOMM | RXFF_RESULT;

    SendRxMsg("AMINETRADIO.1", rxMsg, replyPort);

    if(rxMsg->rm_Result1 == RC_OK
    && rxMsg->rm_Result2)         // Status was received and not empty
        return (STRPTR)rxMsg->rm_Result2;
    else
        return NULL;
}

static VOID SendRxMsg(CONST_STRPTR portName, struct RexxMsg *rxMsg, struct MsgPort *replyPort)
{
    if(FillRexxMsg(rxMsg,1,0))
    {
        struct MsgPort *rxPort;

        Forbid();
    	if((rxPort = FindPort(portName)))
    		PutMsg(rxPort,&rxMsg->rm_Node);
        Permit();

        if(rxPort)
        {
    		WaitPort(replyPort);
    		GetMsg(replyPort);
        }

    	ClearRexxMsg(rxMsg,1);
	}
}

/* /// */
