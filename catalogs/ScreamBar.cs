## Version $VER: ScreamBar.catalog 1.1 (12.04.2018)
## Languages english fran�ais nederlands deutsch
## Codeset english 0
## Codeset fran�ais 0
## Codeset nederlands 0
## Codeset deutsch 0
## SimpleCatConfig CharsPerLine 200
## Header Locale_Strings
## TARGET C english "Cubic:Workspace/ScreamBar/Locale_Strings.h" NoCode
## TARGET CATALOG fran�ais "Cubic:Workspace/ScreamBar/bin/gcc-morphos/Catalogs/fran�ais/" Optimize
## TARGET CATALOG nederlands "Cubic:Workspace/ScreamBar/bin/gcc-morphos/Catalogs/nederlands/" Optimize
## TARGET CATALOG deutsch "Cubic:Workspace/ScreamBar/bin/gcc-morphos/Catalogs/deutsch/" Optimize
MSG_AMINETRADIO_NOT_RUNNING_REQ
\ecAmiNetRadio is not running!
\ecAmiNetRadio n'est pas en train de tourner !
\ecAminetRadio is niet actief!
\ecAmiNetRadio l�uft nicht!
;
MSG_PLUGIN_REJECTED_REQ
\ecPlug-in rejected by AmiNetRadio!\nPerhaps there's another one running.
\ecAmiNetRadio a rejet� le plug-in !\nPeut-�tre qu'un autre est d�j� en train de tourner.
\ecDe plugin is verworpen!\nMisschien werkt een andere plugin.
\ecPlug-in von AmiNetRadio abgelehnt!\nVielleicht l�uft schon ein anderes.
;
MSG_CANNOT_CREATE_MESSAGE_PORT_REQ
\ecCould not create message or reply port!
\ecImpossible de cr�er les ports de communication !
\ecHet is onmogelijk om de communicatie poort te maken!
\ecKonnte keine Nachricht oder keinen Antwortport erstellen!
;
MSG_SIGNAL_ALLOCATION_FAILURE_REQ
\ecSignal allocation failure!
\ec�chec de l'allocation du signal du communication !
\ecNiet in geslaagd de communicatie signaal toewijzen!
\ecSignalzuweisung fehlgeschlagen!
;
MSG_PLUGIN_INITIALIZATION_FAILED_REQ
\ecPlug-in initialization failed!
\ec�chec d'initialisation du plug-in !
\ecVerzuimd om de plug-in te initialiseren!
\ecPlugin-Initialisierung fehlgeschlagen!
;
MSG_ABORT_BTN
  Abort\x20\x20
  Abandonner\x20\x20
  Annuleren\x20\x20
  Abbrechen  
;
MSG_OKAY_BTN
\ec\eb  Okay\x20\x20
\ec\eb  Ok\x20\x20
\ec\eb  Ok\x20\x20
\ec\eb  Okay  
;
MSG_QUIT_BTN
\ec  Quit\x20\x20
\ec  Quitter\x20\x20
\ec  Afsluiten\x20\x20
\ec  Beenden  
;
MSG_SCOPE_TITLE_GRP
Wave form
Forme d'onde
Golfvorm
Wellenform
;
MSG_SCOPE_COLOR_LBL
Wave form color:
Couleur de la forme d'onde:
Kleur van de golfvorm:
Wellenformfarbe:
;
MSG_SCOPE_BACKGROUND_COLOR_LBL
Background color:
Couleur du fond :
Achtergrondkleur:
Hintergrundfarbe:
;
MSG_SCOPE_BACKGROUND_TRANSPARENT_CHK
Transparent
Transparent
Transparant
Transparent
;
MSG_SCOPE_TOP_SPACING_LBL
Top spacing:
Marge sup�rieure :
Boven marge:
Abstand oben:
;
MSG_SCOPE_BOTTOM_SPACING_LBL
Bottom spacing:
Marge inf�rieure :
Onder marge:
Abstand unten:
;
MSG_PIXELS_SLIDER_STRING_D_NUM
%ld pixels
%ld pixels
%ld pixels
%ld Pixel
;
MSG_SCOPE_WIDTH_STRING_D_NUM
Width:
Largeur :
wijdte:
Breite:
;
MSG_SCOPE_SLOW_MODE_CHK
Slow mode
Mode lent
Slow-modus
Langsamer Modus
;
MSG_BUTTONS_TITLE_GRP
Buttons
Boutons
Knopen
Schaltfl�chen
;
MSG_BUTTONS_SHOW_PLAYER_BUTTONS_CHK
Display player buttons
Afficher les boutons du lecteur
Laat de speler knoppen
Abspielertasten anzeigen
;
MSG_COPYRIGHT_MESSAGE_STR
Code: Philippe 'OffseT' Rimauro\nGraphics: David 'davebraco' Braconnier\nEnglish translation: Philippe 'OffseT' Rimauro\n\nBug report? Feature request?\nContact me at:
Programmation : Philippe 'OffseT' Rimauro\nGraphismes : David 'davebraco' Braconnier\n\nUne anomalie � remonter ? Une nouvelle fonctionnalit� � sugg�rer ?\nContactez-moi :
Code: Philippe 'OffseT' Rimauro\nGrafiek: David 'davebraco' Braconnier\nNederlandse vertaling: Xavier 'Alta�r' Lerot\n\nEen bug te rapporten ? Een nieuwe functie voor te stellen? \nNeem contact met \
mij:
Code: Philippe 'OffseT' Rimauro\nGrafik: David 'davebraco' Braconnier\nDeutsche �bersetzung: Stefan A. Haubenthal\n\nBug-Report? Verbesserungsvorschlag?\nKontaktiere mich unter:
;
MSG_UNKNOWN_NAME_STR
\eiUnnamed\en
\eiInd�fini\en
\eiOnbepaald\en
\eiOhne Namen\en
;
MSG_UNKNOWN_TITLE_STR
\eiUnnamed\en
\eiNon renseign�\en
\eiGeen inlichtigen\en
\eiOhne Namen\en
;
MSG_UNKNOWN_ARTIST_STR
\eiUnknown\en
\eiInconnu\en
\eiOnbekend\en
\eiUnbekannt\en
;
MSG_UNKNOWN_ALBUM_STR
\eiNone\en
\eiAucun\en
\eiGeen\en
\eiKein\en
;
MSG_UNKNOWN_YEAR_STR
\eiUnknown\en
\eiInconnue\en
\eiOnbekend\en
\eiUnbekannt\en
;
MSG_UNKNOWN_GENRE_STR
\eiUndefined\en
\eiInd�fini\en
\eiOnbepaald\en
\eiUndefiniert\en
;
MSG_NOT_PLAYING_STR
Stopped.
Arr�t�.
Gestopt.
Angehalten.
;
MSG_DESCRIPTION_STR
Screen bar plug-in for AmiNetRadio.
Plug-in de barre d'�cran pour AmiNetRadio.
Plug-in voor screen bar AmiNetRadio.
Bildschirmleisten-Plugin f�r AmiNetRadio.
;
MSG_SCOPE_COLOR_HLP
Choose the color to use to draw the wave form.
Choix de la couleur pour tracer la forme d'onde.
Keuze van de kleur van de golfvorm te trekken.
W�hle die Farbe, mit der die Wellenform gezeichnet werden soll.
;
MSG_SCOPE_BACKGROUND_COLOR_HLP
Choose the background color for the wave form.
Choix de la couleur de fond pour la forme d'onde.
Keus van achtergrondkleur voor de golfvorm.
W�hle die Hintergrundfarbe f�r die Wellenform.
;
MSG_SCOPE_BACKGROUND_TRANSPARENT_HLP
Use a transparent background for the scope.
Utiliser un fond transparent pour le vum�tre.
Gebruik een transparante achtergrond voor de vu-meter.
Verwende einen transparenten Hintergrund f�r den Bereich.
;
MSG_SCOPE_TOP_SPACING_HLP
Top spacing for the wave form area.
Marge sup�rieure de la zone d'affichage de la forme d'onde.
Bovenmarge van de weergave van de golfvorm gebied.
Oberer Abstand f�r den Wellenformbereich.
;
MSG_SCOPE_BOTTOM_SPACING_HLP
Bottom spacing for the wave form area.
Marge inf�rieure pour la zone d'affichage de la forme d'onde.
Lagere marge voor de weergave van de golfvorm gebied.
Unterer Abstand f�r den Wellenformbereich.
;
MSG_SCOPE_WIDTH_HLP
Width of the scope wave form area.
Largeur de la zone d'affichage de la forme d'onde.
Breedte van de weergave van de golfvorm gebied.
Breite des Oszi-Wellenformbereichs.
;
MSG_SCOPE_SLOW_MODE_HLP
Slow refresh mode for the wave form to save some CPU time if you do not care about a smooth animation.
Mode de rafra�chissement lent pour la forme d'onde, pour �conomiser du processeur si vous ne vous souciez pas de la fluidit� de l'animation.
Slow-modus voor de golfvorm refresh, bespaart processor als je niet de zorg over de vloeibaarheid van de animatie.
Langsamer Aktualisierungsmodus f�r die Wellenform, um etwas CPU-Zeit zu sparen, wenn man nicht auf fl�ssige Animation Wert legt.
;
MSG_BUTTONS_SHOW_PLAYER_BUTTONS_HLP
Choose if you want to display the player buttons or not.
Choix de l'affichage ou non des boutons de contr�le du lecteur.
Keuze om de bedieningsknoppen op de speler weer te geven.
Legt fest, ob die Abspielertasten angezeigt werden sollen oder nicht.
;
MSG_OKAY_HLP
Close this window.
Fermer cette fen�tre.
Sluit dit venster.
Schlie�t dieses Fenster.
;
MSG_QUIT_HLP
Quit ScreamBar.
Quitter ScreamBar.
Screambar afsluiten.
Beende ScreamBar.
;
MSG_MEDIA_STREAM_STR
\ebStream:\en
\ebFlux :\en
\ebStream:\en
\ebStream:\en
;
MSG_MEDIA_TITLE_STR
\ebTitle:\en
\ebTitre :\en
\ebTitel:\en
\ebTitel:\en
;
MSG_MEDIA_ARTIST_STR
\ebArtist:\en
\ebArtiste :\en
\ebArtiest:\en
\ebK�nstler/-in:\en
;
MSG_MEDIA_ALBUM_STR
\ebAlbum:\en
\ebAlbum :\en
\ebAlbum:\en
\ebAlbum:\en
;
MSG_MEDIA_YEAR_STR
\ebYear:\en
\ebAnn�e :\en
\ebJaar:\en
\ebJahr:\en
;
MSG_MEDIA_GENRE_STR
\ebGenre:\en
\ebGenre :\en
\ebGenre:\en
\ebGenre:\en
;
MSG_NO_URL_STR
http://www.perdu.com
http://www.perdu.com
http://www.perdu.com
http://www.perdu.com
;
MSG_AMINETRADIO_NOT_FOUND_REQ
\ecAmiNetRadio not found!\nIs ScreamBar installed in AmiNetRadio plugins drawer?
\ecAmiNetRadio est introuvable !\nScreamBar est-il bien install� dans le tiroir plugins d'AmiNetRadio ?
\ecAmiNetRadio is niet gevonden!\nIs ScreamBar ge�nstalleerd in de lade plugins van AmiNetRadio?
\ecAmiNetRadio nicht gefunden!\nIst ScreamBar in der Schublade f�r AmiNetRadio-Plugins installiert?
;
MSG_AMINETRADIO_NOT_LAUNCHED_REQ
\ecAmiNetRadio could not be started!\nThere is something wrong with your installation!
\ecAmiNetRadio n'a pas pu �tre lanc� !\nQuelque chose ne va pas avec votre installation !
\ecAmiNetRadio kon niet worden gestart!\nEr is iets mis met uw installatie!
\ecAmiNetRadio konnte nicht gestartet werden!\nEs ist etwas mit Ihrer Installation nicht in Ordnung!
;
MSG_WRONG_PARAMETERS_REQ
\ecProvided parameters not recongnized.
\ecLes param�tres fournis n'ont pas �t� reconnus.
\ecDe opgegeven parameters zijn niet herkend.
\ec�bermittelte Parameter nicht erkannt.
;
