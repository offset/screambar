/********************************************************************
 *                                                                  *
 * ScreamBar - Screenbar for AmiNetRadio                            *
 * by OffseT of Futurs' using SimpleCat (c) 2006 by Guido Mersmann  *
 *                                                                  *
 ** SimpleLocale.h ********************** Simple Locale Management **/

#ifndef _SIMPLELOCALE_H_
#define _SIMPLELOCALE_H_

/* /// "Includes" */

#ifndef EXEC_TYPES_H
#include <exec/types.h>
#endif
#ifndef LIBRARIES_LOCALE_H
#include <libraries/locale.h>
#endif
#ifndef LIBRARIES_GADTOOLS_H
#include <libraries/gadtools.h>
#endif

#define CATCOMP_NUMBERS
#define CATCOMP_BLOCK
#include "Locale_Strings.h"

/* /// */

/* /// "Externs" */

extern struct Locale *locale_locale;

/* /// */

/* /// "Prototypes" */

BOOL LocaleOpen(STRPTR catname, ULONG version, ULONG revision);
VOID LocaleClose();

STRPTR GetString(LONG id);
VOID LocalizeMenu(struct NewMenu menu[]);
VOID LocalizeStringArray(STRPTR array[]);

/* /// */

#endif
